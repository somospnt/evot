SET FOREIGN_KEY_CHECKS = 0;
DELETE FROM candidato;
DELETE FROM categoria;
DELETE FROM evento;
DELETE FROM voto;
DELETE FROM usuario;
SET FOREIGN_KEY_CHECKS = 1;

INSERT INTO evento (id, nombre, fecha_desde, fecha_hasta) VALUES
(1, 'Premios Oscar 2015', '2015-02-12 18:00:00.000', '2015-02-22 18:00:00.000'),
(2, 'Premios Oscar 2016', '2016-01-08 00:00:00.000', '2016-02-28 18:00:00.000');


INSERT INTO categoria (id, nombre, descripcion, evento_id) VALUES
(1, 'Mejor Película', '', 2),
(2, 'Mejor Actor', '', 2),
(3, 'Mejor Actriz', '', 2),
(4, 'Mejor Actor Secundario', '', 2),
(5, 'Mejor Actriz Secundaria', '', 2),
(6, 'Mejor Película Animada', '', 2),
(7, 'Mejor Cinematografía', '', 2),
(8, 'Mejor Diseño Vestuario', '', 2),
(9, 'Mejor Director', '', 2),
(10, 'Mejor Documental', '', 2),
(11, 'Mejor Documental (corto)', '', 2),
(12, 'Mejor Edición de Video', '', 2),
(13, 'Mejor Película Extranjera', '', 2),
(14, 'Mejor Maquillaje', '', 2),
(15, 'Mejor Banda Sonora', '', 2),
(16, 'Mejor Canción Original', '', 2),
(17, 'Mejor Diseño de Producción', '', 2),
(18, 'Mejor Corto Animado', '', 2),
(19, 'Mejor Corto de Acción', '', 2),
(20, 'Mejor Edición de Sonido', '', 2),
(21, 'Mejor Mezcla de Sonido', '', 2),
(22, 'Mejor Efectos Visuales', '', 2),
(23, 'Mejor Guión Adaptado', '', 2),
(24, 'Mejor Guión Original', '', 2);


INSERT INTO candidato (id, nombre, descripcion, imagen, categoria_id, ganador) VALUES
-- Mejor pelicula
(1, 'American Sniper', 'Clint Eastwood, Robert Lorenz, Andrew Lazar, Bradley Cooper and Peter Morgan', 'https://image.tmdb.org/t/p/w185/ooy5M7QXEWVpOTAZIRGMskBQbQ9.jpg', 1, 0),
(2, 'Birdman', 'Alejandro G. Iñárritu, John Lesher and James W. Skotchdopole', 'https://image.tmdb.org/t/p/w185/mrJdgLU6xeh00Iy6GpCT4KFH8mb.jpg', 1, 0),
(3, 'Boyhood', 'Richard Linklater and Cathleen Sutherland', 'https://image.tmdb.org/t/p/w185/37zSKwfqzFotm2eiuI268PTCzOj.jpg', 1, 0),
(4, 'The Grand Budapest Hotel', 'Wes Anderson, Scott Rudin, Steven Rales and Jeremy Dawson', 'https://image.tmdb.org/t/p/w185/4hUl6umJmi3UTwnzasBwF0LVlGA.jpg', 1, 0),
(5, 'The Imitation Game', 'Nora Grossman, Ido Ostrowsky and Teddy Schwarzman', 'https://image.tmdb.org/t/p/w185/noUp0XOqIcmgefRnRZa1nhtRvWO.jpg', 1, 0),
(6, 'Selma', 'Christian Colson, Oprah Winfrey, Dede Gardner and Jeremy Kleiner', 'https://image.tmdb.org/t/p/w185/xj1H3dpEHhouyJNB1lfpkKf9fXX.jpg', 1, 0),
(7, 'The Theory of Everything', 'Tim Bevan, Eric Fellner, Lisa Bruce and Anthony McCarten', 'https://image.tmdb.org/t/p/w185/4jspr8hLLuju59bCnMiefzRW4p0.jpg', 1, 0),
(8, 'Whiplash', 'Jason Blum, Helen Estabrook and David Lancaster', 'https://image.tmdb.org/t/p/w185/h82vBRV6yY2Gl38J9laPwq4XfhB.jpg', 1, 0),
-- Mejor Actor
(9, 'Steve Carell', 'Foxcatcher', 'https://image.tmdb.org/t/p/w185/70BJ9xbfkRtEWBeuMcAH8C9lhpA.jpg', 2, 0),
(10, 'Bradley Cooper', 'American Sniper', 'https://image.tmdb.org/t/p/w185/ifjdzZtkR5S5ifSSNQZsVarqFxD.jpg', 2, 0),
(11, 'Benedict Cumberbatch', 'The Imitation Game', 'https://image.tmdb.org/t/p/w185/u4HDP1pGDV4gQZsBgYHKdgfICZz.jpg', 2, 0),
(12, 'Michael Keaton', 'Birdman', 'https://image.tmdb.org/t/p/w185/niT4arrUKdtaxacmxL4IcUWYE6u.jpg', 2, 0),
(13, 'Eddie Redmayne', 'The Theory of Everything', 'https://image.tmdb.org/t/p/w185/grB1xp10baoSVZnb4CC854ys94O.jpg', 2, 0),
-- Mejor Actriz
(14, 'Marion Cotillard', 'Two Days One Night', 'https://image.tmdb.org/t/p/w185/mJl7ngstco78rgxSAwLCPhTEOh5.jpg', 3, 0),
(15, 'Felicity Jones', 'The Theory of Everything', 'https://image.tmdb.org/t/p/w185/gU3lCDCdiY1uAVGFFyTcSQ3M7E8.jpg', 3, 0),
(16, 'Julianne Moore', 'Still Alice', 'https://image.tmdb.org/t/p/w185/eFwyXOKBjKKyfwhGi33FWPc6y5v.jpg', 3, 0),
(17, 'Rosamund Pike', 'Gone Girl', 'https://image.tmdb.org/t/p/w185/70C0zcKqBdiLskAU9FNFtsrpr8m.jpg', 3, 0),
(18, 'Reese Witherspoon', 'Wild', 'https://image.tmdb.org/t/p/w185/6OPIR8TFcdVhox2obxmIWmJ2Qel.jpg', 3, 0),
-- Mejor Actor secundario
(19, 'Robert Duvall', 'The Judge', 'https://image.tmdb.org/t/p/w185/1aBC7NxPy10ofng6HsJBecJ1vMZ.jpg', 4, 0),
(20, 'Ethan Hawke', 'Boyhood', 'https://image.tmdb.org/t/p/w185/kcby6VYk6Gb0036nUyh8chY5ZAJ.jpg', 4, 0),
(21, 'Edward Norton', 'Birdman', 'https://image.tmdb.org/t/p/w185/iUiePUAQKN4GY6jorH9m23cbVli.jpg', 4, 0),
(22, 'Mark Ruffalo', 'Foxcatcher', 'https://image.tmdb.org/t/p/w185/isQ747u0MU8U9gdsNlPngjABclH.jpg', 4, 0),
(23, 'J.K. Simmons', 'Whiplash', 'https://image.tmdb.org/t/p/w185/f2D5wGCqF9t4xsXuaUtIusVKDc1.jpg', 4, 0),
--Mejor Actriz secundaria
(24, 'Patricia Arquette', 'Boyhood', 'https://image.tmdb.org/t/p/w185/9Sz0M91CHHkJ5tlPteiXv34gpgK.jpg', 5, 0),
(25, 'Laura Dern', 'Wild', 'https://image.tmdb.org/t/p/w185/6UdGooksu7F4qfXkdvsQkhFVuhK.jpg', 5, 0),
(26, 'Keira Knightley', 'The Imitation Game', 'https://image.tmdb.org/t/p/w185/b0caJEMemER98VGJTdgV436XJYR.jpg', 5, 0),
(27, 'Emma Stone', 'Birdman', 'https://image.tmdb.org/t/p/w185/lPyR5mKhdqFmgxaCMJK6w3g1OAT.jpg', 5, 0),
(28, 'Meryl Streep', 'Into the Woods', 'https://image.tmdb.org/t/p/w185/oTJj6bLpbmseLww03MOn0eDqYuh.jpg', 5, 0),
-- Mejor Pelicula Animada
(29, 'Big Hero 6', 'Don Hall, Chris Williams and Roy Conli', 'https://image.tmdb.org/t/p/w185/7XB9m8za6wxunWgbNzkGWMY4W8G.jpg', 6, 0),
(30, 'The Boxtrolls', 'Anthony Stacchi, Graham Annable and Travis Knight', 'https://image.tmdb.org/t/p/w185/x7Td7tcK7u1K7OY3zzOK0Hr7S6R.jpg', 6, 0),
(31, 'How to Train Your Dragon 2', 'Dean DeBlois and Bonnie Arnold', 'https://image.tmdb.org/t/p/w185/nnjCR6uOhMJlJbqoMPdOqTLSJaN.jpg', 6, 0),
(32, 'Song of the Sea', 'Tomm Moore and Paul Young', 'https://image.tmdb.org/t/p/w185/szFdh08j9T0RfS84cspCTOeansE.jpg', 6, 0),
(33, 'The Tale of Princess Kaguya', 'Isao Takahata and Yoshiaki Nishimura', 'https://image.tmdb.org/t/p/w185/zFbWL8ujpfaTB3xFGjPRIIxfbyc.jpg', 6, 0),
-- Mejor Cinematografia
(34, 'Emmanuel Lubezki', 'Birdman', 'https://image.tmdb.org/t/p/w185/cUlQjVraYILk8Lt3uCSAJApNYHy.jpg', 7, 0),
(35, 'Robert Yeoman', 'The Grand Budapest Hotel', null, 7, 0),
(36, 'Lukasz Zal and Ryszard Lenczewski', 'Ida', null, 7, 0),
(37, 'Dick Pope', 'Mr.Turner', null, 7, 0),
(38, 'Roger Deakins', 'Unbroken', null, 7, 0),
--Mejor Diseño de vestuario
(39, 'Milena Canonero', 'The Grand Budapest Hotel', NULL, 8, 0),
(40, 'Mark Bridges', 'Inherent Vice', NULL, 8, 0),
(41, 'Colleen Atwood', 'Into the Woods', 'https://image.tmdb.org/t/p/w185/oD91okMgAqHT9qpcBuLYaOJtIBE.jpg', 8, 0),
(42, 'Anna B. Sheppard and Jane Clive', 'Maleficent', NULL, 8, 0),
(43, 'Jacqueline Durran', 'Mr. Turner', 'https://image.tmdb.org/t/p/w185/1AV9wJhLv7WSx4b7fjNPXZMj2CN.jpg', 8, 0),
-- Mejor Director
(44, 'Alexandro G. Iñárritu', 'Birdman', 'https://image.tmdb.org/t/p/w185/qh6Bom66huviIJ2HheE29kI7y96.jpg', 9, 0),
(45, 'Richard Linklater', 'Boyhood', 'https://image.tmdb.org/t/p/w185/ypiAch9og80uE5hnVBAyXXtY4B1.jpg', 9, 0),
(46, 'Bennett Miller', 'Foxcatcher', 'https://image.tmdb.org/t/p/w185/jlnO6Hg6jChoGWr6ScVBpLJBAiI.jpg', 9, 0),
(47, 'Wes Anderson', 'The Grand Budapest Hotel', 'https://image.tmdb.org/t/p/w185/r6mr3gvbuocMznHXSlXVKDj7mEI.jpg', 9, 0),
(48, 'Morten Tyldum', 'The Imitation Game', 'https://image.tmdb.org/t/p/w185/Jjdf8X3cdW5TQebY7xwREkHCCk.jpg', 9, 0),
-- Mejor Documental
(49, 'Citizenfour', 'Laura Poitras, Mathilde Bonnefoy and Dirk Wilutzky', 'https://image.tmdb.org/t/p/w185/yNuLhb2y6I5cO7BfiJ7bdfllnIG.jpg', 10, 0),
(50, 'Finding Vivien Maier', 'John Maloof and Charlie Siskel', NULL, 10, 0),
(51, 'Last Days of Vietnam', 'Rory Kennedy and Keven McAlester', NULL, 10, 0),
(52, 'The Salt of the Earth', 'Wim Wenders, Juliano Ribeiro Salgado and David Rosier', 'https://image.tmdb.org/t/p/w185/imOn4aOv1oJoHBOyA1RUB7Vc1Vp.jpg', 10, 0),
(53, 'Virunga', 'Orlando von Einsiedel and Joanna Natasegara', 'https://image.tmdb.org/t/p/w185/gzGI89tek2VYTyfXBfi8hTg6Kj.jpg', 10, 0),
-- Mejor Documental (corto)
(54, 'Crisis Hotline: Veterans Press 1', 'Ellen Goosenberg Kent and Dana Perry', 'https://image.tmdb.org/t/p/w185/q8v5HdnBLNgtPAHG4WghsiRbLcX.jpg', 11, 0),
(55, 'Joanna', 'Aneta Kopacz', 'https://image.tmdb.org/t/p/w185/rHsztx4Ft3fCrgoQFC7lz97lW5e.jpg', 11, 0),
(56, 'Our Curse', 'Tomasz Sliwinski and Maciej Slesicki', 'https://image.tmdb.org/t/p/w185/3SyK1mcRWjPVzCoDtn6UgtflSc4.jpg', 11, 0),
(57, 'The Reaper', 'Gabriel Serra Arguello', 'https://image.tmdb.org/t/p/w185/su6d64rj3E7FitVxf23fAdm5jmR.jpg', 11, 0),
(58, 'White Earth', 'J. Christian Jensen', 'https://image.tmdb.org/t/p/w185/cIyzaJKIShtF5vbrzDfUBrUnPMJ.jpg', 11, 0),
-- Mejor Edicion de Video
(59, 'Joel Cox and Gary D. Roach', 'American Sniper', null, 12, 0),
(60, 'Sandra Adair', 'Boyhood', null, 12, 0),
(61, 'Barney Pilling', 'The Grand Budapest Hotel', null, 12, 0),
(62, 'William Goldenberg', 'The Imitation Game', null, 12, 0),
(63, 'Tom Cross', 'Whiplash', null, 12, 0),
-- Mejor Pelicula Extranjera
(64, 'Ida', 'Polonia', 'https://image.tmdb.org/t/p/w185/owuEnB96MB2G9exs5jlVGMHNN6c.jpg', 13, 0),
(65, 'Leviathan', 'Rusia', 'https://image.tmdb.org/t/p/w185/8I2Ustn5drYI4pbELhToJvxEUHk.jpg', 13, 0),
(66, 'Tangerines', 'Estonia', 'https://image.tmdb.org/t/p/w185/h4EMWGC4ZvxtpXPdeEXK6qtNkle.jpg', 13, 0),
(67, 'Timbuktu', 'Mauritania', 'https://image.tmdb.org/t/p/w185/fIKhitfpXsACUz36NCf4oecvMRo.jpg', 13, 0),
(68, 'Wild Tales', 'Argentina', 'https://image.tmdb.org/t/p/w185/cC5HpbfC23MkTTYawN5JrpOEIRn.jpg', 13, 0),
-- Mejor maquillaje
(69, 'Foxcatcher', 'Bill Corso and Dennis Liddiard', 'https://image.tmdb.org/t/p/w185/c19ywhw8cxAre6Y43WbWiXSoVCj.jpg', 14, 0),
(70, 'The Grand Budapest Hotel', 'Frances Hannon and Mark Coulier', 'https://image.tmdb.org/t/p/w185/4hUl6umJmi3UTwnzasBwF0LVlGA.jpg', 14, 0),
(71, 'Guardians of the Galaxy', 'Elizabeth Yianni-Georgiou and David White', 'https://image.tmdb.org/t/p/w185/oQpW7EBu1U95JDcUCbtu3sTF8pe.jpg', 14, 0),
-- Mejor Banda Sonora
(72, 'The Grand Budapest Hotel', 'Alexandre Desplat', 'https://image.tmdb.org/t/p/w185/4hUl6umJmi3UTwnzasBwF0LVlGA.jpg', 15, 0),
(73, 'The Imitation Game', 'Alexandre Desplat', 'https://image.tmdb.org/t/p/w185/noUp0XOqIcmgefRnRZa1nhtRvWO.jpg', 15, 0),
(74, 'Interstellar', 'Hans Zimmer', 'https://image.tmdb.org/t/p/w185/7C0oiPn46OvaMxET9iq1f5BsyMS.jpg', 15, 0),
(75, 'Mr. Turner', 'Gary Yershon', 'https://image.tmdb.org/t/p/w185/uZkqc8bHT0XPQrCKZV8QAtmT4cI.jpg', 15, 0),
(76, 'The Theory of Everything', 'Jóhann Jóhannsson', 'https://image.tmdb.org/t/p/w185/i69GWzltZca4w5YEUsHdCwd3SrY.jpg', 15, 0),
--Mejor Canción Original
(77, 'Lost Stars', 'Begin Again', 'https://image.tmdb.org/t/p/w185/mFNO4HF5g5jHXy2H5XZkjqFj0eE.jpg', 16, 0),
(78, 'Everything is Awesome', 'The LEGO Movie', 'https://image.tmdb.org/t/p/w185/hQ66pMhUGHZSChDobQdfbFBduEj.jpg', 16, 0),
(79, 'Glory', 'Selma', 'https://image.tmdb.org/t/p/w185/xj1H3dpEHhouyJNB1lfpkKf9fXX.jpg', 16, 0),
(80, 'Grateful', 'Beyond the Lights', 'https://image.tmdb.org/t/p/w185/vyDDIMbyImLyYUdcc8arWQHvMUP.jpg', 16, 0),
(81, 'I’m Not Gonna Miss You', 'Glen Campbell: I’ll Be Me', null, 16, 0),
-- Mejor Diseño Produccion
(82, 'The Grand Budapest Hotel', 'Adam Stockhausen (Production Design); Anna Pinnock (Set Decoration)', 'https://image.tmdb.org/t/p/w185/4hUl6umJmi3UTwnzasBwF0LVlGA.jpg', 17, 0),
(83, 'The Imitation Game', 'Maria Djurkovic (Production Design); Tatiana Macdonald (Set Decoration)', 'https://image.tmdb.org/t/p/w185/noUp0XOqIcmgefRnRZa1nhtRvWO.jpg', 17, 0),
(84, 'Interstellar', 'Nathan Crowley (Production Design); Gary Fettis (Set Decoration)', 'https://image.tmdb.org/t/p/w185/7C0oiPn46OvaMxET9iq1f5BsyMS.jpg', 17, 0),
(85, 'Into the Woods', 'Dennis Gassner (Production Design); Anna Pinnock (Set Decoration)', 'https://image.tmdb.org/t/p/w185/eI82YRrgeedoWfP5YxdYoJenUEc.jpg', 17, 0),
(86, 'Mr. Turner', 'Suzie Davies (Production Design); Charlotte Watts (Set Decoration)', 'https://image.tmdb.org/t/p/w185/uZkqc8bHT0XPQrCKZV8QAtmT4cI.jpg', 17, 0),
-- Mejor Corto Animado
(87, 'The Bigger Picture', 'Daisy Jacobs and Christopher Hees', 'https://image.tmdb.org/t/p/w185/plALE2CiFRdu5pfhNJm8mwasXQM.jpg', 18, 0),
(88, 'The Dam Keeper', 'Robert Kondo and Dice Tsutsumi', 'https://image.tmdb.org/t/p/w185/mt2v8MM8A91r6MEOtGw0Bju1yxU.jpg', 18, 0),
(89, 'Feast', 'Patrick Osborne and Kristina Reed', 'https://image.tmdb.org/t/p/w185/7K43bEKwuh0dKBVG3xSRWP88P6y.jpg', 18, 0),
(90, 'Me and My Moulton', 'Torill Kove', 'https://image.tmdb.org/t/p/w185/vVVHanl2CIQcWbzf9eUqkzUu678.jpg', 18, 0),
(91, 'A Single Life', 'Joris Oprins', 'https://image.tmdb.org/t/p/w185/efjeok79pAEH87HiIxKXzxpWRCk.jpg', 18, 0),
-- Mejor corto de accion
(92, 'Aya', 'Oded Binnun and Mihal Brezis', 'https://image.tmdb.org/t/p/w185/AhpLrT40cDos5BruaWATlaUlw6e.jpg', 19, 0),
(93, 'Boogaloo and Graham', 'Michael Lennox and Ronan Blaney', 'https://image.tmdb.org/t/p/w185/qlxQGJDchRRjNuvwPzuv87G1Vot.jpg', 19, 0),
(94, 'Butter Lamp', 'Hu Wei and Julien Féret', 'https://image.tmdb.org/t/p/w185/2Nf2NDPgnORbmlfeplwEu3ketnt.jpg', 19, 0),
(95, 'Parvaneh', 'Talkhon Hamzavi and Stefan Eichenberger', 'https://image.tmdb.org/t/p/w185/q5UmwkkNguMhMHjEOmXZWLrgbSv.jpg', 19, 0),
(96, 'The Phone Call', 'Mat Kirkby and James Lucas', 'https://image.tmdb.org/t/p/w185/xKK9Tht9aJUG4TCdaT9Oqi6ilUj.jpg', 19, 0),
-- Mejor Edición de Sonido
(97, 'American Sniper', 'Alan Robert Murray and Bub Asman', 'https://image.tmdb.org/t/p/w185/ooy5M7QXEWVpOTAZIRGMskBQbQ9.jpg', 20, 0),
(98, 'Birdman', 'Martin Hernández and Aaron Glascock', 'https://image.tmdb.org/t/p/w185/mrJdgLU6xeh00Iy6GpCT4KFH8mb.jpg', 20, 0),
(99, 'The Hobbit: The Battle of the Five Armies', 'Brent Burge and Jason Canovas', 'https://image.tmdb.org/t/p/w185/9yF6gkdha6KQNmAcRiSnAuT9Eil.jpg', 20, 0),
(100, 'Interstellar', 'Richard King', 'https://image.tmdb.org/t/p/w185/7C0oiPn46OvaMxET9iq1f5BsyMS.jpg', 20, 0),
(101, 'Unbroken', 'Becky Sullivan and Andrew DeCristofaro', 'https://image.tmdb.org/t/p/w185/1a4P63ieIIng2V5hQou5AeKWl7o.jpg', 20, 0),
-- Mejor Mezcla de Sonido
(102, 'American Sniper', 'John Reitz, Gregg Rudloff and Walt Martin', 'https://image.tmdb.org/t/p/w185/ooy5M7QXEWVpOTAZIRGMskBQbQ9.jpg', 21, 0),
(103, 'Birdman', 'Jon Taylor, Frank A. Montaño and Thomas Varga', 'https://image.tmdb.org/t/p/w185/mrJdgLU6xeh00Iy6GpCT4KFH8mb.jpg', 21, 0),
(104, 'Interstellar', 'Gary A. Rizzo, Gregg Landaker and Mark Weingarten', 'https://image.tmdb.org/t/p/w185/7C0oiPn46OvaMxET9iq1f5BsyMS.jpg', 21, 0),
(105, 'Unbroken', 'Jon Taylor, Frank A. Montaño and David Lee', 'https://image.tmdb.org/t/p/w185/1a4P63ieIIng2V5hQou5AeKWl7o.jpg', 21, 0),
(106, 'Whiplash', 'Craig Mann, Ben Wilkins and Thomas Curley', 'https://image.tmdb.org/t/p/w185/h82vBRV6yY2Gl38J9laPwq4XfhB.jpg', 21, 0),
-- Mejor Efectos Visuales
(107, 'Captain America: The Winter Soldier', 'Dan DeLeeuw, Russell Earl, Bryan Grill and Dan Sudick', 'https://image.tmdb.org/t/p/w185/wMKAshJDuKAVcmVX5pIVtvYhY3f.jpg', 22, 0),
(108, 'Dawn of the Planet of the Apes', 'Joe Letteri, Dan Lemmon, Daniel Barrett and Erik Winquist', 'https://image.tmdb.org/t/p/w185/33RHWuDL9urDxjFkSkdahkMP7xv.jpg', 22, 0),
(109, 'Guardians of the Galaxy', 'Stephane Ceretti, Nicolas Aithadi, Jonathan Fawkner and Paul Corbould', 'https://image.tmdb.org/t/p/w185/oQpW7EBu1U95JDcUCbtu3sTF8pe.jpg', 22, 0),
(110, 'Interstellar', 'Paul Franklin, Andrew Lockley, Ian Hunter and Scott Fisher', 'https://image.tmdb.org/t/p/w185/7C0oiPn46OvaMxET9iq1f5BsyMS.jpg', 22, 0),
(111, 'X-Men: Days of Future Past', 'Richard Stammers, Lou Pecora, Tim Crosbie and Cameron Waldbauer', 'https://image.tmdb.org/t/p/w185/pNrIFKNFij76RmDglSeBQIACZvd.jpg', 22, 0),
-- Mejor guion original
(112, 'American Sniper', 'Escrito por Jason Hall', 'https://image.tmdb.org/t/p/w185/ooy5M7QXEWVpOTAZIRGMskBQbQ9.jpg', 23, 0),
(113, 'The Imitation Game', 'Escrito por Graham Moore', 'https://image.tmdb.org/t/p/w185/noUp0XOqIcmgefRnRZa1nhtRvWO.jpg', 23, 0),
(114, 'Inherent Vice', 'Escrito para el cine por Paul Thomas Anderson', 'https://image.tmdb.org/t/p/w185/2fCg6FORkgRLpFIKlUg4gULIn1u.jpg', 23, 0),
(115, 'The Theory of Everything', 'Guión de Anthony McCarten', 'https://image.tmdb.org/t/p/w185/i69GWzltZca4w5YEUsHdCwd3SrY.jpg', 23, 0),
(116, 'Whiplash', 'Escrito por Damien Chazelle', 'https://image.tmdb.org/t/p/w185/h82vBRV6yY2Gl38J9laPwq4XfhB.jpg', 23, 0),
-- Mejor guion adaptado
(117, 'Birdman', 'Escrito por Alejandro G. Iñárritu, Nicolás Giacobone, Alexander Dinelaris, Jr. & Armando Bo', 'https://image.tmdb.org/t/p/w185/mrJdgLU6xeh00Iy6GpCT4KFH8mb.jpg', 24, 0),
(118, 'Boyhood', 'Escrito por Richard Linklater', 'https://image.tmdb.org/t/p/w185/37zSKwfqzFotm2eiuI268PTCzOj.jpg', 24, 0),
(119, 'Foxcatcher', 'Escrito por E. Max Frye and Dan Futterman', 'https://image.tmdb.org/t/p/w185/c19ywhw8cxAre6Y43WbWiXSoVCj.jpg', 24, 0),
(120, 'The Grand Budapest Hotel', 'Guión de Wes Anderson; Historia de Wes Anderson & Hugo Guinness', 'https://image.tmdb.org/t/p/w185/4hUl6umJmi3UTwnzasBwF0LVlGA.jpg', 24, 0),
(121, 'Nightcrawler', 'Escrito por Dan Gilroy', 'https://image.tmdb.org/t/p/w185/xJaBI8teHuohtqadQjCeEVqqcEm.jpg', 24, 0);
