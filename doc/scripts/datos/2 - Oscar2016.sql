INSERT INTO categoria (id, nombre, descripcion, evento_id) VALUES
(31, 'Mejor Película', '', 2),
(32, 'Mejor Actor', '', 2),
(33, 'Mejor Actriz', '', 2),
(34, 'Mejor Actor de Reparto', '', 2),
(35, 'Mejor Actriz de Reparto', '', 2),
(36, 'Mejor Película de Animación', '', 2),
(37, 'Mejor Fotografía', '', 2),
(38, 'Mejor Diseño Vestuario', '', 2),
(39, 'Mejor Director', '', 2),
(40, 'Mejor Documental', '', 2),
(41, 'Mejor Documental (corto)', '', 2),
(42, 'Mejor Montaje', '', 2),
(43, 'Mejor Película Extranjera', '', 2),
(44, 'Mejor Maquillaje y Peluquería', '', 2),
(45, 'Mejor Banda Sonora', '', 2),
(46, 'Mejor Canción Original', '', 2),
(47, 'Mejor Diseño de Producción', '', 2),
(48, 'Mejor Corto Animado', '', 2),
(49, 'Mejor Corto de Acción', '', 2),
(50, 'Mejor Edición de Sonido', '', 2),
(51, 'Mejor Sonido', '', 2),
(52, 'Mejor Efectos Visuales', '', 2),
(53, 'Mejor Guión Adaptado', '', 2),
(54, 'Mejor Guión Original', '', 2);

INSERT INTO candidato (id, nombre, descripcion, imagen, categoria_id, ganador) VALUES

(130,'The Big Short',       'Brad Pitt, Dede Gardner, Jeremy Kleiner',                      'https://image.tmdb.org/t/p/w185/p11Ftd4VposrAzthkhF53ifYZRl.jpg',31,0 ), 
(131,'Bridge of Spies',     'Steven Spielberg, Marc Platt, Kritie Mascosko Krieger',        'https://image.tmdb.org/t/p/w185/qzEv4nkZdp1nAfril4KR0tDMyro.jpg',31,0 ), 
(132,'Brooklyn',            'Finola Dwyer, Amanda Posey',                                   'https://image.tmdb.org/t/p/w185/yeA0eyNUmMlvXPkm9AHQfzxOwPy.jpg',31,0 ), 
(133,'Mad Max: Fury Road',  'George Miller, Doug Mitchell',                                 'https://image.tmdb.org/t/p/w185/xHfhQIK4BNlsGv5Ylx8mVq0hJT1.jpg',31,0 ), 
(134,'The Martian' ,        'Simon Kinberg, Ridley Scott, Michael Scheafer, Mark Huffman',  'https://image.tmdb.org/t/p/w185/dZjf78JXg0LAjD5qAOs6wFlc67B.jpg',31,0 ), 
(135,'The Revenant',        'Alejandro González Iñárritu, Steve Golin, Arnon Milchan, Mary Parent, Keith Redmon','https://image.tmdb.org/t/p/w185/oXUWEc5i3wYyFnL1Ycu8ppxxPvs.jpg',31,0 ), 
(136,'Room' ,               'Ed Guiney',                                                    'https://image.tmdb.org/t/p/w185/y3zlBAV5GUDAz4iqi6fcXpwFquQ.jpg',31,0 ), 
(137,'Spotlight',           'Michael Sugar, Steve Golin, Nicole Rocklin, Blye Pagon Faust', 'https://image.tmdb.org/t/p/w185/ngKxbvsn9Si5TYVJfi1EGAGwThU.jpg',31,0 ), 
(138,'Bryan Cranston',      'Trumbo; como Dalton Trumbo',                                   'https://image.tmdb.org/t/p/w185/uwGQELv3FGIGm2KU20tOkcKQ54E.jpg',32,0 ), 
(139,'Matt Damon',          'The Martian; como Mark Watney',                                'https://image.tmdb.org/t/p/w185/eLAWpp5BLbTwjj35MbGzpL0QkWv.jpg',32,0 ), 
(140,'Leonardo DiCaprio',   'The Revenant; como Hugh Glass',                                'https://image.tmdb.org/t/p/w185/jToSMocaCaS5YnuOJVqQ7S7pr4Q.jpg',32,0 ), 
(141,'Michael Fassbender',  'Steve Jobs; como Steve Jobs',                                  'https://image.tmdb.org/t/p/w185/r6dxyfjbpOw4CG2feUhlUOLynUs.jpg',32,0 ),
(142,'Eddie Redmayne',      'The Danish Girl; como Lili Elbe / Einar Wegener',              'https://image.tmdb.org/t/p/w185/dFwtlxQgg2zbhRjORkt92E6S4S2.jpg',32,0 ), 
(143,'Cate Blanchett',      'Carol; como Carol Aird',                                       'https://image.tmdb.org/t/p/w185/sPXVcTGkzkbtLg5LcPsdiHF7tLS.jpg',33,0 ), 
(144,'Brie Larson',         'Room; como Joy "Ma" Newsome',                                  'https://image.tmdb.org/t/p/w185/dUVi7lzI36DvqjN39xL9053P7EO.jpg',33,0 ), 
(145,'Jennifer Lawrence',   'Joy; como Joy Mangano',                                        'https://image.tmdb.org/t/p/w185/wf00R1yhOewunWbB7ncQvb9P3eV.jpg',33,0 ), 
(146,'Charlotte Rampling',   '45 Years; como Kate Mercer',                                  'https://image.tmdb.org/t/p/w185/iuggCphmBfNoUFaUWfpJCTxX4e6.jpg',33,0 ), 
(147,'Saoirse Ronan',       'Brooklyn; como Eilis Lacey',                                   'https://image.tmdb.org/t/p/w185/wrgrdC1GAcVboYBPc1xpnt9d5ay.jpg',33,0 ), 
(148,'Christian Bale',      'The Big Short; como Michael Burry',                            'https://image.tmdb.org/t/p/w185/pPXnqoGD91znz4FwQ6aKuxi6Pcy.jpg',34,0 ), 
(149,'Tom Hardy',           'The Revenant; como John Fitzgerald',                           'https://image.tmdb.org/t/p/w185/ojf3deaZVsZU4achP2ZJM7imqir.jpg',34,0 ), 
(150,'Mark Ruffalo',        'Spotlight; como Michael Rezendes',                             'https://image.tmdb.org/t/p/w185/isQ747u0MU8U9gdsNlPngjABclH.jpg',34,0 ), 
(151,'Mark Rylance',        'Bridge of Spies; como Rudolf Abel',                            'https://image.tmdb.org/t/p/w185/5XrQYvdYqiGUepbgmRQfOltgBRJ.jpg',34,0 ), 
(152,'Sylvester Stallone',  'Creed; como Rocky Balboa',                                     'https://image.tmdb.org/t/p/w185/gnmwOa46C2TP35N7ARSzboTdx2u.jpg',34,0 ), 
(153,'Jennifer Jason Leigh',    'The Hateful Eight; como Daisy Domergue a.k.a. "The Prisoner"','https://image.tmdb.org/t/p/w185/9NPK0X3F6e50mOUfm0J9vJexfbQ.jpg',35,0 ), 
(154,'Rooney Mara',         'Carol; como Therese Belivet',                                  'https://image.tmdb.org/t/p/w185/hV6vuYtrnxWUOFsL0EGv3quK8II.jpg',35,0 ), 
(155,'Rachel McAdams',      'Spotlight; como Sacha Pfeiffer',                               'https://image.tmdb.org/t/p/w185/sx8wcwwXTfcCaZenNBCH99DzU36.jpg',35,0 ), 
(156,'Alicia Vikander',      'La chica danesa; como Gerda Wegener',                         'https://image.tmdb.org/t/p/w185/8qqUmSBTDSDfo9vHdaoy5YMJ6oP.jpg',35,0 ), 
(157,'Kate Winslet',        'Steve Jobs; como Joanna Hoffman',                              'https://image.tmdb.org/t/p/w185/2kh86i0q6y8SeBsMGz0UJ3iHMYD.jpg',35,0 ), 
(158,'Anomalisa',           '',                                                             'https://image.tmdb.org/t/p/w185/h6orfYDihFwZfneMvXyGUeTufeK.jpg',36,0 ), 
(159,'Boy & the World',     '',                                                             'https://image.tmdb.org/t/p/w185/fCJrFkVGPFNxc0rfePvlWpJuj5C.jpg',36,0 ), 
(160,'Inside Out',          '',                                                             'https://image.tmdb.org/t/p/w185/aAmfIX3TT40zUHGcCKrlOZRKC7u.jpg',36,0 ), 
(161,'Shaun the Sheep Movie',      '',                                                      'https://image.tmdb.org/t/p/w185/aOHsNN1p2nuiF9WaMaCNXy0T80J.jpg',36,0 ), 
(162,'Omoide no Marnie',      '',                                                           'https://image.tmdb.org/t/p/w185/wiEzIu4bw15RhB3NT58mCbPgv3R.jpg',36,0 ), 
(168,'Carol',               'Edward Lachman',                                               'https://image.tmdb.org/t/p/w185/rt36JrHigDJMKGiNT9D1wtYC9SG.jpg',37,0 ), 
(169,'The Hateful Eight',   'Robert Richardson',                                            'https://image.tmdb.org/t/p/w185/sGP6ItlINnYUc4hVoxfH1Q1BXq2.jpg',37,0 ), 
(170,'Mad Max: Fury Road',  'John Seale',                                                   'https://image.tmdb.org/t/p/w185/xHfhQIK4BNlsGv5Ylx8mVq0hJT1.jpg',37,0 ), 
(171,'The Revenant',        'Emmanuel Lubezki',                                             'https://image.tmdb.org/t/p/w185/oXUWEc5i3wYyFnL1Ycu8ppxxPvs.jpg',37,0 ), 
(172,'Sicario',             'Roger Deakins',                                                'https://image.tmdb.org/t/p/w185/B51aCXYFKEq0FYSJikss4zZBmq.jpg',37,0 ), 
(173,'Carol',               'Sandy Powell',                                                 'https://image.tmdb.org/t/p/w185/rt36JrHigDJMKGiNT9D1wtYC9SG.jpg',38,0 ), 
(174,'Cinderella',          'Sandy Powell',                                                 'https://image.tmdb.org/t/p/w185/aoE7yPycPtobhF8eMECNNHb8diN.jpg',38,0 ), 
(175,'The Danish Girl',     'Paco Delgado',                                                 'https://image.tmdb.org/t/p/w185/seWQ6UKCrhGH0eP7dFZvmIBQtKF.jpg',38,0 ), 
(176,'Mad Max: Fury Road',  'Jenny Beavan',                                                 'https://image.tmdb.org/t/p/w185/xHfhQIK4BNlsGv5Ylx8mVq0hJT1.jpg',38,0 ), 
(177,'The Revenant',        'Jacqueline West',                                              'https://image.tmdb.org/t/p/w185/oXUWEc5i3wYyFnL1Ycu8ppxxPvs.jpg',38,0 ), 
(178,'Lenny Abrahamson',    'Room',                                                         NULL    ,39,0 ), 
(179,'Alejandro González Iñárritu',     'The Revenant',                                     'https://image.tmdb.org/t/p/w185/lhnQdcTlurlUYxuZvjaQFjUwO3o.jpg',39,0 ), 
(180,'Thomas McCarthy',     'Spotlight',                                                    'https://image.tmdb.org/t/p/w185/8k6Nvp7A21Bbwse5zvLHkJjbdmf.jpg',39,0 ), 
(181,'George Miller',       'Mad Max: Fury Road',                                           'https://image.tmdb.org/t/p/w185/pYSqeP5nhrdEJBCoZm611MhX9kC.jpg',39,0 ), 
(182,'Adam McKay',          'The Big Short',                                                'https://image.tmdb.org/t/p/w185/pwc6pHC5d3y8NVsphURkKK8y2fL.jpg',39,0 ), 
(183,'Amy',                 '',                                                             'https://image.tmdb.org/t/p/w185/qh1NsTkpeoUt3kyJ56p22gDPlvJ.jpg',40,0 ), 
(184,'Cartel Land',         '',                                                             'https://image.tmdb.org/t/p/w185/3DGaP0ZC7Fd5K8mx4NiZHf5hWrK.jpg',40,0 ), 
(185,'The Look of Silence',     '',                                                         'https://image.tmdb.org/t/p/w185/1tTFTYQF7hvMScM1RCsKdgTJaQT.jpg',40,0 ), 
(186,'"What Happened, Miss Simone?"',     '',                                               'https://image.tmdb.org/t/p/w185/dHXeRW71m0KmEuj1XovM7RRVc32.jpg',40,0 ), 
(187,'Winter on Fire',     '',                                                              'https://image.tmdb.org/t/p/w185/orkCXzXmOs4g6CWR7zZqyFZJozq.jpg',40,0 ), 
(188,'Body Team 12',     '',                                                                'https://image.tmdb.org/t/p/w185/6EiGdnO7QJiK6r9Ivp6LOCcUuh0.jpg',41,0 ), 
(189,'Chau, Beyond the Lines','',                                                           'https://image.tmdb.org/t/p/w185/9sbBflpH5iCilrzJqtahOh8FXXk.jpg',41,0 ), 
(190,'Claude Lanzmann: Spectres of the Shoah',     '',                                      'https://image.tmdb.org/t/p/w185/j8OVNalDRn2J44haJOkh3NQ7Exg.jpg',41,0 ), 
(191,'A Girl in the River: The Price of Forgiveness',     '',                               'https://image.tmdb.org/t/p/w185/6EUpLl6gXgZkeWc08Se5Ft6tfoX.jpg',41,0 ), 
(192,'Last Day of Freedom',     '',                                                         'https://image.tmdb.org/t/p/w185/4092VklXmOeFhWfFUueFhcZ7JSB.jpg',41,0 ), 
(193,'The Revenant',     'Stephen Mirrione',                                                'https://image.tmdb.org/t/p/w185/oXUWEc5i3wYyFnL1Ycu8ppxxPvs.jpg',42,0 ), 
(194,'The Big Short',     'Hank Corwin',                                                    'https://image.tmdb.org/t/p/w185/p11Ftd4VposrAzthkhF53ifYZRl.jpg',42,0 ), 
(195,'Mad Max: Fury Road',     'Margaret Sixel',                                            'https://image.tmdb.org/t/p/w185/xHfhQIK4BNlsGv5Ylx8mVq0hJT1.jpg',42,0 ), 
(196,'Spotlight',     'Tom McArdle',                                                        'https://image.tmdb.org/t/p/w185/ngKxbvsn9Si5TYVJfi1EGAGwThU.jpg',42,0 ), 
(197,'Star Wars: The Force Awakens',     'Maryann Brandon, Mary Jo Markey',                 'https://image.tmdb.org/t/p/w185/fqos3bbu4io2sQvERcW8rzGJwq2.jpg',42,0 ), 
(198,'Krigen (A War)',     'Dinamarca',                                                     'https://image.tmdb.org/t/p/w185/xJO6660Vkd2c49PxdTxW1hCwIhm.jpg',43,0 ), 
(199,'El abrazo de la serpiente',     'Colombia',                                           'https://image.tmdb.org/t/p/w185/d9NYzYbBFObfTkyxmVVhVAcMLn2.jpg',43,0 ), 
(200,'Mustang',     'Francia',                                                              'https://image.tmdb.org/t/p/w185/6ZTAEaMvH1f72AXuwLXlDCG0bdi.jpg',43,0 ), 
(201,'Saul fia (Son of Saul)',     'Hungría',                                               'https://image.tmdb.org/t/p/w185/AcjoM9JielY0Yi42GnICNBntpND.jpg',43,0 ), 
(202,'Theeb',     'Jordania',                                                               'https://image.tmdb.org/t/p/w185/8InbAAKXdP8dsKq2b3xmIIt3XKD.jpg',43,0 ), 
(203,'El abuelo que saltó por la ventana y se largó',     '"Love Larson, Eva von Bahr"',    'https://image.tmdb.org/t/p/w185/vCeowDn2IPGbHw7nJIUDEqeuVTt.jpg',44,0 ), 
(204,'The Revenant',     '"Siân Grigg, Duncan Jarman, Robert Pandini"',                     'https://image.tmdb.org/t/p/w185/oXUWEc5i3wYyFnL1Ycu8ppxxPvs.jpg',44,0 ), 
(205,'Mad Max: Fury Road',     '"Damian Martin, Lesley Vanderwalt, Elka Wardega"',          'https://image.tmdb.org/t/p/w185/xHfhQIK4BNlsGv5Ylx8mVq0hJT1.jpg',44,0 ), 
(206,'Bridge of Spies',     'Thomas Newman',                                                'https://image.tmdb.org/t/p/w185/qzEv4nkZdp1nAfril4KR0tDMyro.jpg',45,0 ), 
(207,'Carol ',     'Carter Burwell',                                                        'https://image.tmdb.org/t/p/w185/rt36JrHigDJMKGiNT9D1wtYC9SG.jpg',45,0 ), 
(208,'The Hateful Eight',     'Ennio Morricone',                                            'https://image.tmdb.org/t/p/w185/sGP6ItlINnYUc4hVoxfH1Q1BXq2.jpg',45,0 ), 
(209,'Sicario',     'Jóhann Jóhannsson',                                                    'https://image.tmdb.org/t/p/w185/B51aCXYFKEq0FYSJikss4zZBmq.jpg',45,0 ),
(210,'Star Wars: The Force Awakens',     'John Williams',                                   'https://image.tmdb.org/t/p/w185/fqos3bbu4io2sQvERcW8rzGJwq2.jpg',45,0 ), 
(211,'«Earned It»',     '"Cincuenta sombras de Grey; compuesta por Ahmad Balshe, Stephan Moccio, Jason Daheala Quenneville y Abel Tesfaye"',NULL,46,0 ), 
(212,'«Manta Ray»',     '"Racing Extinction; compuesta por Antony Hegarty, J. Ralph"',      NULL,46,0 ), 
(213,'«Simple Song #3»',     'Youth; compuesta por David Lang',                             NULL,46,0 ), 
(214,'«Til It Happens to You»',     'The Hunting Ground; compuesta por Lady Gaga y Diane Warren',NULL,46,0 ), 
(215,'«Writings on the Wall»',     'Spectre; compuesta por Jimmy Napes y Sam Smith',        NULL,46,0 ), 
(216,'Bridge of Spies',     '"Rena DeAngelo, Bernhard Henrich, Adam Stockhausen"',          'https://image.tmdb.org/t/p/w185/qzEv4nkZdp1nAfril4KR0tDMyro.jpg',47,0 ), 
(217,'The Revenant',     '"Jack Fisk, Hamish Purdy"',                                       'https://image.tmdb.org/t/p/w185/oXUWEc5i3wYyFnL1Ycu8ppxxPvs.jpg',47,0 ), 
(218,'The Danish Girl',     '"Michael Standish, Eve Stewart"',                              'https://image.tmdb.org/t/p/w185/seWQ6UKCrhGH0eP7dFZvmIBQtKF.jpg',47,0 ), 
(219,'Mad Max: Fury Road',     '"Colin Gibson, Lisa Thompson"',                             'https://image.tmdb.org/t/p/w185/xHfhQIK4BNlsGv5Ylx8mVq0hJT1.jpg',47,0 ), 
(220,'The Martian',     '"Celia Bobak, Arthur Max"',                                        'https://image.tmdb.org/t/p/w185/dZjf78JXg0LAjD5qAOs6wFlc67B.jpg',47,0 ), 
(221,'Bear Story',     '',                                                                  'https://image.tmdb.org/t/p/w185/eNaYdftWYNpyyqZ6aYWwZ9sAkWR.jpg',48,0 ), 
(222,'Mi ne mozhem zhit bez kosmosa',     '',                                               'https://image.tmdb.org/t/p/w185/yq43UT9XcbSxgDsKfEue8p0KhbQ.jpg',48,0 ), 
(223,'Prologue',     '',                                                                    'https://image.tmdb.org/t/p/w185/kuAR2BRUawTJW1Lbq2pgzPMQpdQ.jpg',48,0 ), 
(224,'Sanjay’s Super Team',     '',                                                          'https://image.tmdb.org/t/p/w185/55N3em0I4NtoQgUA5rzXRFzUHg1.jpg',48,0 ), 
(225,'World of Tomorrow',     '',                                                           'https://image.tmdb.org/t/p/w185/tBCL8Ieywcgs1dFOBramBFOCZtO.jpg',48,0 ), 
(226,'Alles wird gut (Everything will be okay)',     '',                                    'https://image.tmdb.org/t/p/w185/x0tcORtqtt5gw1v6NGQj6XOfArz.jpg',49,0 ), 
(227,'Ave Maria',     '',                                                                   'https://image.tmdb.org/t/p/w185/745Kv4esGa5pcge00tAa8UiqaSs.jpg',49,0 ), 
(228,'Day One',     '',                                                                     'https://image.tmdb.org/t/p/w185/worjSSWmOWismS8kqsHLG4E1hoP.jpg',49,0 ), 
(229,'Shok',     '',                                                                        'https://image.tmdb.org/t/p/w185/iJyDl1sPkwWEqdAeYzVxIOfer55.jpg',49,0 ), 
(230,'Stutterer',     '',                                                                   'https://image.tmdb.org/t/p/w185/8QePBQM3gZEabzlUoAbgGxsPEeT.jpg',49,0 ), 
(231,'The Revenant',     '"Lon Bender, Martin Hernández"',                                  'https://image.tmdb.org/t/p/w185/oXUWEc5i3wYyFnL1Ycu8ppxxPvs.jpg',50,0 ), 
(232,'Mad Max: Fury Road',     '"Mark A. Mangini, David White"',                            'https://image.tmdb.org/t/p/w185/xHfhQIK4BNlsGv5Ylx8mVq0hJT1.jpg',50,0 ), 
(233,'The Martian',     'Oliver Tarney',                                                    'https://image.tmdb.org/t/p/w185/dZjf78JXg0LAjD5qAOs6wFlc67B.jpg',50,0 ), 
(234,'Sicario',     'Alan Robert Murray',                                                   'https://image.tmdb.org/t/p/w185/B51aCXYFKEq0FYSJikss4zZBmq.jpg',50,0 ), 
(235,'Star Wars: The Force Awakens',     '"David Acord, Matthew Wood"',                     'https://image.tmdb.org/t/p/w185/fqos3bbu4io2sQvERcW8rzGJwq2.jpg',50,0 ), 
(236,'Bridge of Spies',     '"Drew Kunin, Andy Nelson, Gary Rydstrom"',                     'https://image.tmdb.org/t/p/w185/qzEv4nkZdp1nAfril4KR0tDMyro.jpg',51,0 ), 
(237,'The Revenant',     '"Chris Duesterdiek, Jon Taylor, Frank A. Montaño, Randy Thom"',   'https://image.tmdb.org/t/p/w185/oXUWEc5i3wYyFnL1Ycu8ppxxPvs.jpg',51,0 ), 
(238,'Mad Max: Fury Road',     '"Chris Jenkins, Ben Osmo, Gregg Rudloff"',                  'https://image.tmdb.org/t/p/w185/xHfhQIK4BNlsGv5Ylx8mVq0hJT1.jpg',51,0 ), 
(239,'The Martian',     '"Paul Massey, Mac Ruth, Mark Taylor"',                             'https://image.tmdb.org/t/p/w185/dZjf78JXg0LAjD5qAOs6wFlc67B.jpg',51,0 ), 
(240,'Star Wars: The Force Awakens',     '"Andy Nelson, Christopher Scarabosio, Stuart Wilson"','https://image.tmdb.org/t/p/w185/fqos3bbu4io2sQvERcW8rzGJwq2.jpg',51,0 ), 
(241,'The Revenant',     '"Richard McBride, Matt Shumway, Jason Smith, Cameron Waldbauer"', 'https://image.tmdb.org/t/p/w185/oXUWEc5i3wYyFnL1Ycu8ppxxPvs.jpg',52,0 ), 
(242,'Ex Machina',     '"Mark Williams Ardington, Sara Bennett, Paul Norris, Andrew Whitehurst"','https://image.tmdb.org/t/p/w185/94gszl6b8BKC3RvoHDsgL4MDO6J.jpg',52,0 ), 
(243,'Mad Max: Fury Road',     '"Andrew Jackson, Dan Oliver, Andy Williams, Tom Wood"',     'https://image.tmdb.org/t/p/w185/xHfhQIK4BNlsGv5Ylx8mVq0hJT1.jpg',52,0 ), 
(244,'The Martian',     '"Anders Langlands, Chris Lawrence, Richard Stammers, Steven Warner"','https://image.tmdb.org/t/p/w185/dZjf78JXg0LAjD5qAOs6wFlc67B.jpg',52,0 ), 
(245,'Star Wars: The Force Awakens',     '"Chris Corbould, Roger Guyett, Paul Kavanagh, Neal Scanlan"','https://image.tmdb.org/t/p/w185/fqos3bbu4io2sQvERcW8rzGJwq2.jpg',52,0 ), 
(246,'Brooklyn',     'Nick Hornby',                                                         'https://image.tmdb.org/t/p/w185/yeA0eyNUmMlvXPkm9AHQfzxOwPy.jpg',53,0 ), 
(247,'Carol',     'Phyllis Nagy',                                                           'https://image.tmdb.org/t/p/w185/rt36JrHigDJMKGiNT9D1wtYC9SG.jpg',53,0 ), 
(248,'The Big Short',     '"Adam McKay, Charles Randolph"',                                 'https://image.tmdb.org/t/p/w185/p11Ftd4VposrAzthkhF53ifYZRl.jpg',53,0 ), 
(249,'Room',     'Emma Donoghue',                                                           'https://image.tmdb.org/t/p/w185/y3zlBAV5GUDAz4iqi6fcXpwFquQ.jpg',53,0 ), 
(250,'The Martian',     'Drew Goddard',                                                            'https://image.tmdb.org/t/p/w185/dZjf78JXg0LAjD5qAOs6wFlc67B.jpg',53,0 ), 
(251,'Bridge of Spies',     '"Matt Charman, Ethan Coen, Joel Coen"',                               'https://image.tmdb.org/t/p/w185/qzEv4nkZdp1nAfril4KR0tDMyro.jpg',54,0 ), 
(252,'Ex machina',     'Alex Garland',                                                             'https://image.tmdb.org/t/p/w185/94gszl6b8BKC3RvoHDsgL4MDO6J.jpg',54,0 ), 
(253,'Inside Out',     '"Josh Cooley, Ronnie del Carmen, Pete Docter, Meg LeFauve"',               'https://image.tmdb.org/t/p/w185/kuAr6WShAqRpcd0qwYeo90xyRiC.jpg',54,0 ), 
(254,'Spotlight',     '"Tom McCarthy, Josh Singer"',                                               'https://image.tmdb.org/t/p/w185/ngKxbvsn9Si5TYVJfi1EGAGwThU.jpg',54,0 ), 
(255,'Straight Outta Compton',     '"Andrea Berloff, Jonathan Herman, S. Leigh Savidge, Alan Wenkus"','https://image.tmdb.org/t/p/w185/5LmdnQJHU5EPmbNqNaULbhIIz7X.jpg',54,0);