#------------------------------------------------------------------------------
#--- Dropear todas las tablas del esquema. Mas info: http://stackoverflow.com/questions/12403662/drop-all-tables
#------------------------------------------------------------------------------
SET FOREIGN_KEY_CHECKS = 0;
SET @tables = NULL;
SELECT GROUP_CONCAT(table_schema, '.', table_name) INTO @tables
  FROM information_schema.tables
  WHERE table_schema = 'evot'; -- specify DB name here.

SET @tables = CONCAT('DROP TABLE ', @tables);
PREPARE stmt FROM @tables;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
SET FOREIGN_KEY_CHECKS = 1;

CREATE TABLE  evento (
  id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(255) NOT NULL,
  fecha date NOT NULL
);


CREATE TABLE categoria (
  id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(255) NOT NULL,
  descripcion text NOT NULL,
  evento_id int(11) NOT NULL,
   FOREIGN KEY (evento_id) REFERENCES evento(id)
);

CREATE TABLE candidato (
  id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(255) NOT NULL,
  descripcion text NOT NULL,
  imagen varchar(500),
  categoria_id int(11) NOT NULL,
  ganador tinyint(1) NOT NULL,
  FOREIGN KEY (categoria_id) REFERENCES categoria(id)
);


CREATE TABLE  usuario (
  id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(255) NOT NULL,
  email varchar(255) NOT NULL UNIQUE
);

CREATE TABLE  voto (
  id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  usuario_id int(11) NOT NULL,
  candidato_id int(11) NOT NULL,
  FOREIGN KEY (usuario_id) REFERENCES usuario(id),
  FOREIGN KEY (candidato_id) REFERENCES candidato(id)
);

alter table usuario add column fecha_creacion datetime;
update usuario set fecha_creacion = '2015-02-12 18:00:00.000';
alter table usuario modify fecha_creacion datetime not null;

-- Agregar columnas "fecha_desde" y "fecha_hasta" al evento.

alter table evento change column fecha fecha_desde datetime not null;
update evento set fecha_desde = '2015-02-12 18:00:00.000' where id = 1;

alter table evento add column fecha_hasta datetime;
update evento set fecha_hasta = '2015-02-22 18:00:00.000' where id = 1;
alter table evento change column fecha_hasta fecha_hasta datetime not null;

