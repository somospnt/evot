package com.somospnt.evot.sevice;

import com.somospnt.evot.ApplicationConfig;
import com.somospnt.evot.domain.Evento;
import com.somospnt.evot.service.EventoService;
import com.somospnt.evot.vo.RankingVo;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationConfig.class)
@ActiveProfiles("test")
@WebAppConfiguration
public class EventoServiceTest {

    @Autowired
    private EventoService eventoService;

    @Test
    public void obtenerUno_idEventoExistente_devuelveEventoCompleto() {
        Long idEventoOscar = new Long(1);

        Evento oscar = eventoService.obtenerUno(idEventoOscar);

        assertNotNull(oscar);
        assertEquals("Premios Oscar", oscar.getNombre());
    }

    @Test
    public void obtenerUno_idEventoInexistente_devuelveNull() {
        Long idEventoOscar = new Long(5);

        Evento oscar = eventoService.obtenerUno(idEventoOscar);

        assertNull(oscar);
    }

    @Test
    public void buscarRanking_conIdEventoCorrecto_retornaRanking(){
        Long idEvento = 1L;

        List<RankingVo> ranking = eventoService.buscarRanking(idEvento);

        assertNotNull(ranking);
        assertFalse(ranking.isEmpty());
    }
}
