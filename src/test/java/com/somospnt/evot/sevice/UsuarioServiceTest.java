package com.somospnt.evot.sevice;

import com.somospnt.evot.ApplicationConfig;
import com.somospnt.evot.domain.Usuario;
import com.somospnt.evot.repository.UsuarioRepository;
import com.somospnt.evot.service.UsuarioService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationConfig.class)
@ActiveProfiles("test")
@WebAppConfiguration
public class UsuarioServiceTest {

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private UsuarioRepository usuarioRepository;


    @Test
    public void crear_conUsuarioInexistente_creaUsuario() {

        Usuario usuario = new Usuario();
        usuario.setNombre("Lio Messi");
        usuario.setEmail("lio@barca.es");
        usuarioService.crear(usuario);

        Usuario usuarioRegistrado = usuarioRepository.findByEmail("lio@barca.es");
        assertNotNull(usuarioRegistrado);
        assertEquals("Lio Messi", usuarioRegistrado.getNombre());
    }

    @Test(expected = IllegalArgumentException.class)
    public void crear_conUsuarioYaExistente_lanzaError() {

        Usuario usuario = new Usuario();
        usuario.setNombre("Lio Messi");
        usuario.setEmail("lio@barca.es");

        usuarioService.crear(usuario);
    }

    @Test
    public void obtenerPorId_conIdDeUsuarioExistente_retornaUsuarioCompleto() {
        Usuario usuario = usuarioService.obtenerPorId(1L);

        assertNotNull(usuario);
        assertEquals(new Long(1L), usuario.getId());
    }

    @Test
    public void obtenerPorEmail_emailExiste_retornaUsuario() {
        String email = "ldeseta@gmail.com";
        Usuario usuario = usuarioService.obtenerPorEmail(email);
        assertNotNull(usuario);
        assertEquals(email, usuario.getEmail());
    }

    @Test
    public void obtenerPorEmail_emailNoExiste_retornaNull() {
        String email = "usuario-no-existe@gmail.com";
        Usuario usuario = usuarioService.obtenerPorEmail(email);
        assertNull(usuario);
    }


}
