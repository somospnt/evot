package com.somospnt.evot.sevice;

import com.somospnt.evot.ApplicationConfig;
import com.somospnt.evot.domain.Candidato;
import com.somospnt.evot.domain.Usuario;
import com.somospnt.evot.domain.Voto;
import com.somospnt.evot.repository.UsuarioRepository;
import com.somospnt.evot.repository.VotoRepository;
import com.somospnt.evot.service.CandidatoService;
import com.somospnt.evot.service.VotoService;
import com.somospnt.evot.vo.VotoVo;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationConfig.class)
@ActiveProfiles("test")
@WebAppConfiguration
@Transactional
public class VotoServiceTest {

    @Autowired
    private CandidatoService candidatoService;

    @Autowired
    private VotoService votoService;

    @Autowired
    private VotoRepository votoRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Test
    public void registrarVotos_sinHaberVotadoEnEventoActivo_creaVotosUsuario() {
        VotoVo votoVo = new VotoVo();
        Usuario usuario = new Usuario();
        usuario.setNombre("Roger Federer");
        usuario.setEmail("roger@wimby.uk");

        Candidato candidatoUno = candidatoService.obtenerUno(123L);
        Candidato candidatoDos = candidatoService.obtenerUno(124L);
        Candidato candidatoTres = candidatoService.obtenerUno(125L);

        List<Candidato> candidatos = new ArrayList<>();
        candidatos.add(candidatoUno);
        candidatos.add(candidatoDos);
        candidatos.add(candidatoTres);

        votoVo.setUsuario(usuario);
        votoVo.setCandidatos(candidatos);
        long idEvento = 2L;
        votoVo.setIdEvento(idEvento);

        votoService.registrar(votoVo);

        Iterable<Voto> votos = votoRepository.findAll();
        for (Voto voto : votos) {
            if (voto.getCandidato().getCategoria().getEvento().getId() == idEvento) {
                assertEquals("roger@wimby.uk", voto.getUsuario().getEmail());
            }
        }
    }


    @Test(expected = IllegalArgumentException.class)
    public void registrarVotos_sinHaberVotadoEnEventoVencido_lanzaExcepcion() {
        VotoVo votoVo = new VotoVo();
        Usuario usuario = new Usuario();
        usuario.setNombre("Roger Federer");
        usuario.setEmail("roger2@wimby.uk");

        Candidato candidatoUno = candidatoService.obtenerUno(130L);

        List<Candidato> candidatos = new ArrayList<>();
        candidatos.add(candidatoUno);

        votoVo.setUsuario(usuario);
        votoVo.setCandidatos(candidatos);
        long idEvento = 4L;
        votoVo.setIdEvento(idEvento);

        votoService.registrar(votoVo);
    }

    @Test(expected = IllegalArgumentException.class)
    public void registrarVotos_sinHaberVotadoEnEventoFuturo_lanzaExcepcion() {
        VotoVo votoVo = new VotoVo();
        Usuario usuario = new Usuario();
        usuario.setNombre("Roger Federer");
        usuario.setEmail("roger@wimby.uk");

        Candidato candidatoUno = candidatoService.obtenerUno(128L);

        List<Candidato> candidatos = new ArrayList<>();
        candidatos.add(candidatoUno);

        votoVo.setUsuario(usuario);
        votoVo.setCandidatos(candidatos);
        long idEvento = 3L;
        votoVo.setIdEvento(idEvento);

        votoService.registrar(votoVo);
    }



    @Test(expected = IllegalArgumentException.class)
    public void registrarVotos_conMasDeUnVotoPorCategoria_lanzaError() {
        VotoVo votoVo = new VotoVo();
        Usuario usuario = new Usuario();
        usuario.setNombre("Roger Federer");
        usuario.setEmail("roger@wimby.uk");

        Candidato candidatoUno = candidatoService.obtenerUno(124L);  //categoria 32
        Candidato candidatoDos = candidatoService.obtenerUno(125L);  //categoria 33
        Candidato candidatoTres = candidatoService.obtenerUno(126L); //categoria 33

        List<Candidato> candidatos = new ArrayList<>();
        candidatos.add(candidatoUno);
        candidatos.add(candidatoDos);
        candidatos.add(candidatoTres);

        votoVo.setUsuario(usuario);
        votoVo.setCandidatos(candidatos);
        votoVo.setIdEvento(2L);

        votoService.registrar(votoVo);
    }

    @Test(expected = IllegalArgumentException.class)
    public void registrar_conUsuarioYaExistente_lanzaError() {
        VotoVo votoVo = new VotoVo();
        Usuario usuario = new Usuario();
        usuario.setNombre("Di Maria");
        usuario.setEmail("dimaria@manu.uk");

        Candidato candidatoUno = candidatoService.obtenerUno(123L);

        List<Candidato> candidatos = new ArrayList<>();
        candidatos.add(candidatoUno);

        votoVo.setUsuario(usuario);
        votoVo.setCandidatos(candidatos);
        votoVo.setIdEvento(2L);

        votoService.registrar(votoVo);
    }

    @Test(expected = IllegalArgumentException.class)
    public void registrar_conVoracionIncompleta_lanzaError() {
        VotoVo votoVo = new VotoVo();
        Usuario usuario = new Usuario();
        usuario.setNombre("Di Maria");
        usuario.setEmail("dimaria@manu.uk");

        Candidato candidatoUno = candidatoService.obtenerUno(123L);
        Candidato candidatoDos = candidatoService.obtenerUno(124L);

        List<Candidato> candidatos = new ArrayList<>();
        candidatos.add(candidatoUno);
        candidatos.add(candidatoDos);

        votoVo.setUsuario(usuario);
        votoVo.setCandidatos(candidatos);
        votoVo.setIdEvento(2L);

        votoService.registrar(votoVo);
    }

    @Test
    public void registrar_conIdCandidatoErroneo_RollbackTransaccionCompleta() {
        long cantAntUsr = usuarioRepository.count();
        try {

            VotoVo votoVo = new VotoVo();
            Usuario usuario = new Usuario();
            usuario.setNombre("Di Maria");
            usuario.setEmail("dimaria@manu.uk");

            Candidato candidato = new Candidato();
            candidato.setId(Long.MAX_VALUE);
            List<Candidato> candidatos = new ArrayList<>();
            candidatos.add(candidato);

            votoVo.setUsuario(usuario);
            votoVo.setCandidatos(candidatos);
            votoVo.setIdEvento(2L);

            votoService.registrar(votoVo);
        } catch (Exception e) {
            long cantActUsr = usuarioRepository.count();
            assertEquals(cantAntUsr, cantActUsr);
        }

    }

    @Test(expected = IllegalArgumentException.class)
    public void registrar_conUsuarioSinEmail_lanzaException() {
        VotoVo votoVo = new VotoVo();
        Usuario usuario = new Usuario();
        usuario.setNombre("Di Maria");
        votoVo.setUsuario(usuario);

        votoService.registrar(votoVo);
    }

    @Test(expected = IllegalArgumentException.class)
    public void registrar_conUsuarioSinNombre_lanzaException() {
        VotoVo votoVo = new VotoVo();
        Usuario usuario = new Usuario();
        usuario.setEmail("Di Maria@com.com");
        votoVo.setUsuario(usuario);

        votoService.registrar(votoVo);
    }

    @Test(expected = IllegalArgumentException.class)
    public void registrar_sinUsuario_lanzaException() {
        VotoVo votoVo = new VotoVo();

        votoService.registrar(votoVo);
    }
}
