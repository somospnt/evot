DROP TABLE IF EXISTS voto;
DROP TABLE IF EXISTS usuario;
DROP TABLE IF EXISTS candidato;
DROP TABLE IF EXISTS categoria;
DROP TABLE IF EXISTS evento;

CREATE TABLE IF NOT EXISTS evento (
  id        BIGINT IDENTITY PRIMARY KEY,
  nombre    varchar(255) NOT NULL,
  fecha_desde     date NOT NULL,
  fecha_hasta     date NOT NULL
);

CREATE TABLE IF NOT EXISTS categoria (
  id            BIGINT IDENTITY PRIMARY KEY,
  nombre        varchar(255) NOT NULL,
  descripcion   varchar(255) NOT NULL,
  evento_id     BIGINT NOT NULL,
  FOREIGN KEY (evento_id) REFERENCES evento (id)
);

CREATE TABLE IF NOT EXISTS candidato (
  id            BIGINT IDENTITY PRIMARY KEY,
  nombre        varchar(255) NOT NULL,
  descripcion   varchar(255) NOT NULL,
  imagen        varchar(255) NOT NULL,
  categoria_id  BIGINT NOT NULL,
  ganador       BIT DEFAULT 0,
  FOREIGN KEY (categoria_id) REFERENCES categoria (id)
);

CREATE TABLE IF NOT EXISTS usuario (
  id            BIGINT IDENTITY PRIMARY KEY,
  nombre        varchar(255) NOT NULL,
  email         varchar(255) NOT NULL UNIQUE,
  fecha_creacion     date NOT NULL
);

CREATE TABLE IF NOT EXISTS voto (
  id            BIGINT IDENTITY PRIMARY KEY,
  usuario_id    BIGINT NOT NULL,
  candidato_id  BIGINT NOT NULL,
  FOREIGN KEY (usuario_id) REFERENCES usuario (id),
  FOREIGN KEY (candidato_id) REFERENCES candidato (id)
);

INSERT INTO evento (id, nombre, fecha_desde, fecha_hasta) VALUES
(1, 'Premios Oscar',         '2014-02-01', '2014-02-22'),      -- este evento ya está vencido en los tests
(2, 'Premios Gardel',        '2015-01-20', '2500-04-20'),      -- este evento no vence en los tests
(3, 'Premios Martin Fierro', '2500-03-20', '2500-04-20'),      -- este evento empieza en el futuro en los tests
(4, 'Premios Vencidos',      '2010-03-20', '2010-04-20');      -- este evento ya esta vencido en los tests

INSERT INTO categoria (id, nombre, descripcion, evento_id) VALUES
(2, 'Mejor Película', '', 1),
(3, 'Mejor Director', '', 1),
(4, 'Mejor Actriz', '', 1),
(8, 'Mejor Cinematografía', '', 1),
(11, 'Mejor Película Extranjera', '', 1),
(12, 'Mejor Guión Adaptado', '', 1),
(13, 'Mejor Guión Original', '', 1),
(14, 'Mejor Maquillaje', '', 1),
(15, 'Mejor Banda Sonora', '', 1),
(16, 'Mejor Canción Original', '', 1),
(17, 'Mejor Película Animada', '', 1),
(18, 'Mejor Documental (corto)', '', 1),
(19, 'Mejor Edición de Video', '', 1),
(20, 'Mejor Diseño de Producción', '', 1),
(21, 'Mejor Corto Animado', '', 1),
(22, 'Mejor Corto de Acción', '', 1),
(23, 'Mejor Edición de Sonido', '', 1),
(24, 'Mejor Mezcla de Sonido', '', 1),
(25, 'Mejor Efectos Visuales', '', 1),
(26, 'Mejor Documental', '', 1),
(27, 'Mejor Actor Secundario', '', 1),
(28, 'Mejor Actriz Secundaria', '', 1),
(29, 'Mejor Diseño Vestuario', '', 1),
(30, 'Mejor Actor', '', 1),
(31, 'Mejor banda', '', 2),
(32, 'Mejor cantante', '', 2),
(33, 'Mejor video', '', 2),
(34, 'Categoria evento futuro', '', 3),
(35, 'Categoria evento vencido', '', 4);


INSERT INTO candidato (id, nombre, descripcion, imagen, categoria_id, ganador) VALUES
(1, 'American Sniper', '', '', 2, 0),
(2, 'Birdman', '', '', 2, 0),
(3, 'Boyhood', '', '', 2, 1),
(4, 'The Grand Budapest Hotel', '', '', 2, 1),
(5, 'The Imitation Game', '', '', 2, 0),
(6, 'Selma', '', '', 2, 0),
(7, 'The Theory of Everything', '', '', 2, 0),
(8, 'Whiplash', '', '', 2, 0),
(9, 'Alexandro G. Iñárritu, Birdman', '', '', 3, 0),
(10, 'Richard Linklater, Boyhood', '', '', 3, 1),
(11, 'Bennett Miller, Foxcatcher', '', '', 3, 0),
(12, 'Wes Anderson, The Grand Budapest Hotel', '', '', 3, 0),
(13, 'Morten Tyldum, The Imitation Game', '', '', 3, 1),
(15, 'Steve Carell', 'Foxcatcher', '', 30, 0),
(16, 'Bradley Cooper', 'American Sniper', '', 30, 0),
(17, 'Benedict Cumberbatch', 'The Imitation Game', '', 30, 0),
(18, 'Michael Keaton', 'Birdman', '', 30, 0),
(19, 'Eddie Redmayne', 'The Theory of Everything', '', 30, 0),
(20, 'Marion Cotillard', 'Two Days One Night', '', 4, 1),
(21, 'Felicity Jones', 'The Theory of Everything', '', 4, 0),
(22, 'Julianne Moore', 'Still Alice', '', 4, 0),
(23, 'Rosamund Pike', 'Gone Girl', '', 4, 0),
(24, 'Reese Witherspoon', 'Wild', '', 4, 0),
(25, 'Emmanuel Lubezki', 'Birdman', '', 8, 1),
(26, 'Robert Yeoman', 'TheGrandBudapestHotel', '', 8, 0),
(27, 'LukaszZalandRyszardLenczewski', 'Ida', '', 8, 1),
(28, 'DickPope', 'Mr.Turner', '', 8, 0),
(29, 'RogerDeakins', 'Unbroken', '', 8, 0),
(30, 'Ida', '', '', 11, 0),
(31, 'Leviathan', '', '', 11, 1),
(32, 'Tangerines', '', '', 11, 1),
(33, 'Timbuktu', '', '', 11, 0),
(34, 'Wild Tales', '', '', 11, 0),
(35, 'American Sniper', '', '', 12, 1),
(36, 'The Imitation Game', '', '', 12, 0),
(37, 'Inherent Vice', '', '', 12, 1),
(38, 'The Theory of Everything', '', '', 12, 0),
(39, 'Whiplash', '', '', 12, 0),
(40, 'Birdman', '', '', 13, 0),
(41, 'Boyhood', '', '', 13, 1),
(42, 'Foxcatcher', '', '', 13, 0),
(43, 'The Grand Budapest Hotel', '', '', 13, 0),
(44, 'Nightcrawler', '', '', 13, 0),
(45, 'Foxcatcher', '', '', 14, 0),
(46, 'The Grand Budapest Hotel', '', '', 14, 1),
(47, 'Guardians of the Galaxy', '', '', 14, 1),
(48, 'The Grand Budapest Hotel', '', '', 15, 1),
(49, 'The Imitation Game', '', '', 15, 1),
(50, 'Interstellar', '', '', 15, 0),
(51, 'Mr. Turner', '', '', 15, 0),
(52, 'The Theory of Everything', '', '', 15, 0),
(53, 'Lost Stars', 'Begin Again', '', 16, 0),
(54, 'Everything is Awesome', 'The LEGO Movie', '', 16, 0),
(55, 'Glory', 'Selma', '', 16, 1),
(56, 'Grateful', 'Beyond the Lights', '', 16, 1),
(57, 'I’m Not Gonna Miss You', 'Glen Campbell…I’ll Be Me', '', 16, 0),
(58, 'Big Hero 6', '', '', 17, 0),
(59, 'The Boxtrolls', '', '', 17, 0),
(60, 'How to Train Your Dragon 2', '', '', 17, 0),
(61, 'Song of the Sea', '', '', 17, 0),
(62, 'The Tale of Princess Kaguya', '', '', 17, 0),
(63, 'Crisis Hotline: Veterans Press 1', '', '', 18, 0),
(64, 'Joanna', '', '', 18, 0),
(65, 'Our Curse', '', '', 18, 0),
(66, 'The Reaper', '', '', 18, 0),
(67, 'White Earth', '', '', 18, 0),
(68, 'Joel Cox and Gary D. Roach', 'American Sniper', '', 19, 0),
(69, 'Sandra Adair', 'Boyhood', '', 19, 0),
(70, 'Barney Pilling', 'The Grand Budapest Hotel', '', 19, 0),
(71, 'William Goldenberg', 'The Imitation Game', '', 19, 0),
(72, 'Tom Cross', 'Whiplash', '', 19, 0),
(73, 'The Grand Budapest Hotel', '', '', 20, 0),
(74, 'The Imitation Game', '', '', 20, 0),
(75, 'Interstellar', '', '', 20, 0),
(76, 'Into the Woods', '', '', 20, 0),
(77, 'Mr. Turner', '', '', 20, 0),
(78, 'The Bigger Picture', '', '', 21, 0),
(79, 'The Dam Keeper', '', '', 21, 0),
(80, 'Feast', '', '', 21, 0),
(81, 'Me and My Moulton', '', '', 21, 0),
(82, 'A Single Life', '', '', 21, 0),
(83, 'Aya', '', '', 22, 0),
(84, 'Boogaloo and Graham', '', '', 22, 0),
(85, 'Butter Lamp', '', '', 22, 0),
(86, 'Parvaneh', '', '', 22, 0),
(87, 'The Phone Call', '', '', 22, 0),
(88, 'American Sniper', '', '', 23, 0),
(89, 'Birdman', '', '', 23, 0),
(90, 'The Hobbit: The Battle of the Five Armies', '', '', 23, 0),
(91, 'Interstellar', '', '', 23, 0),
(92, 'Unbroken', '', '', 23, 0),
(93, 'American Sniper', '', '', 24, 0),
(94, 'Birdman', '', '', 24, 0),
(95, 'Interstellar', '', '', 24, 0),
(96, 'Unbroken', '', '', 24, 0),
(97, 'Whiplash', '', '', 24, 0),
(98, 'Captain America: The Winter Soldier', '', '', 25, 0),
(99, 'Dawn of the Planet of the Apes', '', '', 25, 0),
(100, 'Guardians of the Galaxy', '', '', 25, 0),
(101, 'Interstellar', '', '', 25, 0),
(102, 'X-Men: Days of Future Past', '', '', 25, 0),
(103, 'Citizenfour', '', '', 26, 0),
(104, 'Finding Vivien Maier', '', '', 26, 0),
(105, 'Last Days of Vietnam', '', '', 26, 0),
(106, 'The Salt of the Earth', '', '', 26, 0),
(107, 'Virunga', '', '', 26, 0),
(108, 'Robert Duvall', 'The Judge', '', 27, 0),
(109, 'Ethan Hawke', 'Boyhood', '', 27, 0),
(110, 'Edward Norton', 'Birdman', '', 27, 0),
(111, 'Mark Ruffalo', 'Foxcatcher', '', 27, 0),
(112, 'J.K. Simmons', 'Whiplash', '', 27, 0),
(113, 'Patricia Arquette', 'Boyhood', '', 28, 0),
(114, 'Laura Dern', 'Wild', '', 28, 0),
(115, 'Keira Knightley', 'The Imitation Game', '', 28, 0),
(116, 'Emma Stone', 'Birdman', '', 28, 0),
(117, 'Meryl Streep', 'Into the Woods', '', 28, 0),
(118, 'Milena Canonero', 'The Grand Budapest Hotel', '', 29, 0),
(119, 'Mark Bridges', 'Inherent Vice', '', 29, 0),
(120, 'Colleen Atwood', 'Into the Woods', '', 29, 0),
(121, 'Anna B. Sheppard and Jane Clive', 'Maleficent', '', 29, 0),
(122, 'Jacqueline Durran', 'Mr. Turner', '', 29, 0),
(123, 'Sumo', 'Into the Woods', '', 31, 0),
(124, 'Luca', 'Maleficent', '', 32, 0),
(125, 'Video', 'Mr. Turner', '', 33, 0),
(126, 'Video', 'Mr. Turner 2', '', 33, 0),
(127, 'Participante 1', 'Mr. Turner 2', '', 34, 0),
(128, 'Participante 2', 'Descripcoon', '', 34, 0),
(129, 'Participante 1', 'Descripcoon', '', 35, 0),
(130, 'Participante 2', 'Descripcoon', '', 35, 0);

INSERT INTO usuario (id, nombre, email,fecha_creacion) VALUES
(1, 'Roman', 'romanglom@gmail.com',sysdate),
(2, 'Leo', 'ldeseta@gmail.com',sysdate),
(3, 'Di Maria', 'dimaria@manu.uk',sysdate);

INSERT INTO voto (usuario_id, candidato_id)
	VALUES (1, 3);
INSERT INTO voto (usuario_id, candidato_id)
	VALUES (1, 11);
INSERT INTO voto (usuario_id, candidato_id)
	VALUES (1, 20);
INSERT INTO voto (usuario_id, candidato_id)
	VALUES (1, 25);
INSERT INTO voto (usuario_id, candidato_id)
	VALUES (1, 31);
INSERT INTO voto (usuario_id, candidato_id)
	VALUES (1, 37);
INSERT INTO voto (usuario_id, candidato_id)
	VALUES (1, 41);
INSERT INTO voto (usuario_id, candidato_id)
	VALUES (1, 46);
INSERT INTO voto (usuario_id, candidato_id)
	VALUES (1, 48);
INSERT INTO voto (usuario_id, candidato_id)
	VALUES (1, 55);
INSERT INTO voto (usuario_id, candidato_id)
	VALUES (2, 4);
INSERT INTO voto (usuario_id, candidato_id)
	VALUES (2, 13);
INSERT INTO voto (usuario_id, candidato_id)
	VALUES (2, 20);
INSERT INTO voto (usuario_id, candidato_id)
	VALUES (2, 27);
INSERT INTO voto (usuario_id, candidato_id)
	VALUES (2, 32);
INSERT INTO voto (usuario_id, candidato_id)
	VALUES (2, 35);
INSERT INTO voto (usuario_id, candidato_id)
	VALUES (2, 41);
INSERT INTO voto (usuario_id, candidato_id)
	VALUES (2, 47);
INSERT INTO voto (usuario_id, candidato_id)
	VALUES (2, 49);
INSERT INTO voto (usuario_id, candidato_id)
	VALUES (2, 56);