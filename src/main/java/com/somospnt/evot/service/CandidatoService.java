package com.somospnt.evot.service;

import com.somospnt.evot.domain.Candidato;

public interface CandidatoService {

    Candidato obtenerUno(Long id);
}
