package com.somospnt.evot.service;

import com.somospnt.evot.domain.Usuario;

public interface UsuarioService {

    void crear(Usuario usuario);

    Usuario obtenerPorId(Long id);

    Usuario obtenerPorEmail(String email);
}
