package com.somospnt.evot.service;

import com.somospnt.evot.domain.Evento;
import com.somospnt.evot.vo.RankingVo;
import java.util.List;

public interface EventoService {

    Evento obtenerUno(long id);
    
    List<RankingVo> buscarRanking(long idEvento);
}
