package com.somospnt.evot.service;

import com.somospnt.evot.domain.Voto;
import com.somospnt.evot.vo.VotoVo;
import java.util.List;

public interface VotoService {

    void registrar(VotoVo votoVo);

    List<Voto> obtenerPorUsuarioId(Long id);
}
