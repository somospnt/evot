package com.somospnt.evot.controller;

import com.somospnt.evot.service.VotoService;
import com.somospnt.evot.vo.VotoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VotoRestController {

    @Autowired
    private VotoService votoService;

    @RequestMapping(value = "/api/votos", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void votar(@RequestBody VotoVo votoVo) {
        votoService.registrar(votoVo);

    }

}
