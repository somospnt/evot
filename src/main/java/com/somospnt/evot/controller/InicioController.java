package com.somospnt.evot.controller;

import com.somospnt.evot.domain.Evento;
import com.somospnt.evot.service.EventoService;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class InicioController {

    @Autowired
    private EventoService eventoService;

    @RequestMapping("/")
    public String oscar2016(Model model) {
        Evento oscar2016 = eventoService.obtenerUno(2L);
        model.addAttribute("oscar2016", oscar2016);

        Date ahora = new Date();
        //si esta cerrado
        if (ahora.after(oscar2016.getFechaHasta())) {
            return "redirect:/eventos/" + oscar2016.getId() + "/ranking";
        } else {
            return "front-oscar-2016";
        }

    }

    @RequestMapping("/bases-y-condiciones")
    public String basesYCondiciones(Model model) {
        return "front-bases-y-condiciones";
    }
}
