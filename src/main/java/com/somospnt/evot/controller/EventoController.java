package com.somospnt.evot.controller;

import com.somospnt.evot.domain.Evento;
import com.somospnt.evot.domain.Usuario;
import com.somospnt.evot.domain.Voto;
import com.somospnt.evot.service.EventoService;
import com.somospnt.evot.service.UsuarioService;
import com.somospnt.evot.service.VotoService;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class EventoController {

    @Autowired
    private EventoService eventoService;
    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private VotoService votoService;

    @RequestMapping("/eventos/{idEvento}/ranking")
    public String ranking(@PathVariable long idEvento, @RequestParam(required = false) String email, Model model) {
        model.addAttribute("evento", eventoService.obtenerUno(idEvento));
        model.addAttribute("ranking", eventoService.buscarRanking(idEvento));
        model.addAttribute("usuario", usuarioService.obtenerPorEmail(email));
        return "front-ranking";
    }

    @RequestMapping("/eventos/{idEvento}/usuarios/{idUsuario}")
    public String usuario(Model model, @PathVariable Long idEvento, @PathVariable Long idUsuario) {
        Usuario usuario = usuarioService.obtenerPorId(idUsuario);
        List<Voto> votos = votoService.obtenerPorUsuarioId(idUsuario);
        Evento evento = eventoService.obtenerUno(idEvento);
        model.addAttribute("usuario", usuario);
        model.addAttribute("votos", votos);
        model.addAttribute("evento", evento);

        Date ahora = new Date();
        if (ahora.after(evento.getFechaHasta())) {
            model.addAttribute("eventoTerminado", true);
        } else {
            model.addAttribute("eventoTerminado", false);
        }

        return "front-usuario";
    }
}
