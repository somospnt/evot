package com.somospnt.evot.controller;

import com.somospnt.evot.domain.Usuario;
import com.somospnt.evot.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class VotoController {

    @Autowired
    private UsuarioService usuarioService;

    @RequestMapping("/votos/usuarios")
    public String mostrarVotosPorEmail(@RequestParam String email, @RequestParam Long eventoId, Model model) {
        Usuario usuario = usuarioService.obtenerPorEmail(email);
        if (usuario != null) {
            return "redirect:/eventos/" + eventoId + "/usuarios/" + usuario.getId();
        }
        model.addAttribute("mensajeBuscadorVotos", "¡Todavía no votaste!");
        return "forward:/";

    }

}
