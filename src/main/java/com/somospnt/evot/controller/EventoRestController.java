package com.somospnt.evot.controller;

import com.somospnt.evot.domain.Evento;
import com.somospnt.evot.service.EventoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EventoRestController {

    @Autowired
    private EventoService eventoService;

    @RequestMapping(value = "/api/eventos/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Evento obtenerUno(@PathVariable(value = "id") Long id) {
        return eventoService.obtenerUno(id);
    }
}
