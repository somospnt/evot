package com.somospnt.evot.vo;

import com.somospnt.evot.domain.Candidato;
import com.somospnt.evot.domain.Usuario;
import java.util.List;

public class VotoVo {

    private Usuario usuario;
    private List<Candidato> candidatos;
    private Long idEvento;

    /**
     * @return the usuario
     */
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the candidatos
     */
    public List<Candidato> getCandidatos() {
        return candidatos;
    }

    /**
     * @param candidatos the candidatos to set
     */
    public void setCandidatos(List<Candidato> candidatos) {
        this.candidatos = candidatos;
    }

    public Long getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(Long idEvento) {
        this.idEvento = idEvento;
    }



}
