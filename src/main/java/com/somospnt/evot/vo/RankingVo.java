package com.somospnt.evot.vo;

import com.somospnt.evot.domain.Usuario;

public class RankingVo {
    
    private Usuario usuario;
    private long cantidadVotos;

    public RankingVo(Usuario usuario, long cantidadVotos) {
        this.usuario = usuario;
        this.cantidadVotos = cantidadVotos;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public long getCantidadVotos() {
        return cantidadVotos;
    }

    public void setCantidadVotos(long cantidadVotos) {
        this.cantidadVotos = cantidadVotos;
    }
    
}
