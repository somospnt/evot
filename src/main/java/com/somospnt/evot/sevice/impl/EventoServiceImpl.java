package com.somospnt.evot.sevice.impl;

import com.somospnt.evot.domain.Evento;
import com.somospnt.evot.repository.EventoRepository;
import com.somospnt.evot.repository.VotoRepository;
import com.somospnt.evot.service.EventoService;
import com.somospnt.evot.vo.RankingVo;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EventoServiceImpl implements EventoService {

    @Autowired
    private EventoRepository eventoRepository;
    @Autowired
    private VotoRepository votoRepository;

    @Override
    public Evento obtenerUno(long id) {
        return eventoRepository.findOne(id);
    }

    @Override
    public List<RankingVo> buscarRanking(long idEvento) {
        return votoRepository.buscarRanking(idEvento);
    }

}
