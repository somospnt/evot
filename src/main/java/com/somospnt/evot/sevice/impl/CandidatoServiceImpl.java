package com.somospnt.evot.sevice.impl;

import com.somospnt.evot.domain.Candidato;
import com.somospnt.evot.repository.CandidatoRepository;
import com.somospnt.evot.service.CandidatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CandidatoServiceImpl implements CandidatoService {

    @Autowired
    private CandidatoRepository candidatoRepository;

    @Override
    public Candidato obtenerUno(Long id) {
        return candidatoRepository.findOne(id);
    }

}
