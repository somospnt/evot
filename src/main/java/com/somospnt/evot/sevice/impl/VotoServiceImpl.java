package com.somospnt.evot.sevice.impl;

import com.somospnt.evot.domain.Candidato;
import com.somospnt.evot.domain.Categoria;
import com.somospnt.evot.domain.Evento;
import com.somospnt.evot.domain.Voto;
import com.somospnt.evot.repository.CandidatoRepository;
import com.somospnt.evot.repository.EventoRepository;
import com.somospnt.evot.repository.UsuarioRepository;
import com.somospnt.evot.repository.VotoRepository;
import com.somospnt.evot.service.VotoService;
import com.somospnt.evot.vo.VotoVo;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

@Service
@Transactional
public class VotoServiceImpl implements VotoService {

    @Autowired
    private VotoRepository votoRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private EventoRepository eventoRepository;

    @Autowired
    private CandidatoRepository candidatoRepository;

    @Override
    public void registrar(VotoVo votoVo) {

        if ((votoVo.getUsuario() == null) || StringUtils.isEmpty(votoVo.getUsuario().getEmail())
                || StringUtils.isEmpty(votoVo.getUsuario().getNombre())) {
            throw new IllegalArgumentException("Completá los datos obligatorios");
        }

        Evento evento = eventoRepository.findOne(votoVo.getIdEvento());
        Date ahora = new Date();
        if (ahora.before(evento.getFechaDesde()) || ahora.after(evento.getFechaHasta())) {
            throw new IllegalArgumentException("Estas votando en un evento inactivo.");
        }

        if (evento.getCategorias().size() != votoVo.getCandidatos().size()) {
            throw new IllegalArgumentException("Se debe votar un candidato por categoria");
        }

        try {
            usuarioRepository.save(votoVo.getUsuario());
        } catch (Exception exc) {
            throw new IllegalArgumentException("Ya existe una votación para el usuario " + votoVo.getUsuario().getEmail(), exc);
        }

        Map<Long, Categoria> categorias = new HashMap<>();

        votoVo.getCandidatos().stream().map((candidato) -> {
            Candidato unCandidato = candidatoRepository.findOne(candidato.getId());
            Long idCategoria = unCandidato.getCategoria().getId();
            if (categorias.containsKey(idCategoria)) {
                throw new IllegalArgumentException("No se permite más de un voto por categoria");
            }
            categorias.put(unCandidato.getCategoria().getId(), candidato.getCategoria());

            Voto voto = new Voto();
            voto.setCandidato(candidato);
            voto.setUsuario(votoVo.getUsuario());
            return voto;
        }).forEach((voto) -> {
            votoRepository.save(voto);
        });

    }

    @Override
    public List<Voto> obtenerPorUsuarioId(Long id) {
        return votoRepository.findByUsuarioId(id);
    }

}
