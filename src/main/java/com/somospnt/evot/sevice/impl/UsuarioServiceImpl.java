package com.somospnt.evot.sevice.impl;

import com.somospnt.evot.domain.Usuario;
import com.somospnt.evot.repository.UsuarioRepository;
import com.somospnt.evot.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public void crear(Usuario usuario) {

        try {
            usuarioRepository.save(usuario);
        } catch (Exception exc) {
            throw new IllegalArgumentException("Ya existe el usuario registrado", exc);
        }
    }

    @Override
    public Usuario obtenerPorId(Long id) {
        return usuarioRepository.findById(id);
    }

    @Override
    public Usuario obtenerPorEmail(String email) {
        return usuarioRepository.findByEmail(email);
    }

}
