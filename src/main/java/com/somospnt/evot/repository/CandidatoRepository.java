package com.somospnt.evot.repository;


import com.somospnt.evot.domain.Candidato;
import org.springframework.data.repository.CrudRepository;

public interface CandidatoRepository extends CrudRepository<Candidato, Long> {
    
}
