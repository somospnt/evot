package com.somospnt.evot.repository;


import com.somospnt.evot.domain.Categoria;
import org.springframework.data.repository.CrudRepository;

public interface CategoriaRepository extends CrudRepository<Categoria, Long> {

}
