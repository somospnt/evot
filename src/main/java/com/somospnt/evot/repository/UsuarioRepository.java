package com.somospnt.evot.repository;


import com.somospnt.evot.domain.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

    Usuario findById(Long id);

    Usuario findByEmail(String email);
    
    Usuario findByNombre(String nombre);
}
