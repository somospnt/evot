package com.somospnt.evot.repository;

import com.somospnt.evot.domain.Voto;
import com.somospnt.evot.vo.RankingVo;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface VotoRepository extends CrudRepository<Voto, Long> {

    List<Voto> findByUsuarioId(Long id);

    @Query("select new com.somospnt.evot.vo.RankingVo(v.usuario, count(*)) from Voto v "
            + "where v.candidato.categoria.evento.id = ? "
            + "and v.candidato.ganador = true "
            + "group by v.usuario order by 2 desc")
    List<RankingVo> buscarRanking(long idEvento);
}
