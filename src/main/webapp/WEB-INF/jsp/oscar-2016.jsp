<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!-- Intro Section -->
<section id="intro">
    <div class="video-content">
        <div class="video-image wp1 delay-1s">
            <img src="img/video-img.jpg" alt="">
        </div>
        <div class="overlay">
            <div class="container-wrapper">
                <div class="container">
                    <div class="col-md-12 wp1 delay-05s">
                        <div class="intro-info-wrapper">
                            <h1 class="text-center">Oscars <span class="text-color font-light">2016</span></h1>
                            <div class="row hidden-xs">
                                <div class="clock-wrapper"><div class="clock"></div></div>
                            </div>
                            <div class="row text-center">
                                <h3>Votá a los ganadores <br>y participá por entradas para el cine</h3>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="media-btns buttons page-scroll">
                    <a href="#actors">
                        <div class="btn btn-default" id="play-button">Votar<i class="fa fa-play animated"></i></div>
                    </a>
                    <div class="text-center">
                        <a href="${root}/bases-y-condiciones">Ver bases y condiciones</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>


<section id="actors">
    <div class="container"> 
        <div class="row">
            <div class="col-lg-12">
                <div id="owl-categorias" class="owl-carousel owl-theme">
                    <c:forEach items="${oscar2016.categorias}" var="categoria" varStatus="loopStatus">
                        <div class="item" data-categoria-indice="${loopStatus.index}">
                            <h1 class="section-title"><c:out value="${categoria.nombre}"/></h1>
                            <h2><c:out value="${categoria.descripcion}"/></h2>                        
                            <c:forEach items="${categoria.candidatos}" var="candidato">
                                <div class="col-md-4">
                                    <div class="evot-categoria-info row evot-js-categoria" data-id-categoria="<c:out value="${categoria.id}"/>" data-id-candidato="<c:out value="${candidato.id}"/>">
                                        <div class="col-md-4 col-xs-4 evot-imagen">
                                            <c:if test="${fn:length(categoria.candidatos) lt 7}">
                                                <!--segun la cantidad de datos-->
                                            </c:if>
                                            <c:if test="${empty candidato.imagen}">
                                                <img class="img-responsive img-thumbnail" src="http://www.gossipcop.com/wp-content/uploads/2013/02/ABC1-250x323.png">
                                            </c:if>
                                            <c:if test="${not empty candidato.imagen}">
                                                <img class="img-responsive img-thumbnail" src="<c:out value="${candidato.imagen}"/>">
                                            </c:if>
                                        </div>
                                        <div class="col-md-8 col-xs-8">
                                            <h4 class="heading"><c:out value="${candidato.nombre}"/></h4>
                                        </div>
                                        <div class="col-md-8 col-xs-8 descripcion">
                                            <c:out value="${candidato.descripcion}"/>
                                        </div>
                                    </div>                            
                                </div>
                            </c:forEach>
                        </div><!-- /.item -->  
                    </c:forEach>                        
                    <div class="item">         
                        <h1 class="section-title">Último paso</h1>
                        <div class="row">
                            <div class="col-md-offset-3 col-md-6">
                                <h2>Completá tus datos y ya estás participando...</h2>
                                <form class="form" id="voto-form">
                                    <div class="form-group">
                                        <label class="sr-only" for="nombre">Nombre</label>
                                        <input type="text" class="form-control" id="nombre" placeholder="Nombre">
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="email">Email address</label>
                                        <input type="email" class="form-control" id="email" placeholder="Email">
                                    </div>
                                    <button type="submit" class="btn btn-default" id="enviar-votos-boton">Enviar <i class="fa fa-envelope-o animated"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.col-lg-12 -->       
    </div>        
</section>

