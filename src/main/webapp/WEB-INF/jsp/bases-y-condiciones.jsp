<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<section id="about-sub" class="evot-seccion">              
    <div class="container">       
        <div class="row">
            <div class="col-lg-12 col-md-12 wp3 delay-05s animated fadeInDown">
                <h1 class="section-title">Bases y condiciones</h1>  
                <p>
                    Reglamento para la participaci�n del juego de los Oscar 2016.
                </p>
                <ul class="feature-list">
                    <li><i class="fa fa-bullseye"></i> La participaci�n en el concurso es completamente gratuita.</li>
                    <li><i class="fa fa-bullseye"></i> Podr� participar en el concurso la persona que vote en todas las ternas propuestas en la p�gina e ingrese su nombre y email.</li>
                    <li><i class="fa fa-bullseye"></i> El o los ganadores ser�n aquellos que m�s aciertos tengan acorde a la entrega oficial de los Oscar 2016.</li>
                    <li><i class="fa fa-bullseye"></i> El premio ser�n 2 (dos) entradas de cine para el primer ganador y 2 (dos) entradas de cine para el segundo ganador.</li>
                    <li><i class="fa fa-bullseye"></i> El ganador ser� notificado dentro de las 24hs posteriores a la finalizaci�n de la ceremonia de los Oscar 2016.</li>
                    <li><i class="fa fa-bullseye"></i> El premio quedar� vacante en caso que el ganador no responda el email de notificaci�n.</li>
                    <li><i class="fa fa-bullseye"></i> En caso de paridad de aciertos entre varias personas, se realizar� un sorteo utilizando el sitio random.org</li>
                    <li><i class="fa fa-bullseye"></i> Las salas de cine seleccionadas para el premio deber�n estar ubicadas en CABA.</li>
                    <li><i class="fa fa-bullseye"></i> El cierre de las votaciones se realizar� una hora antes del comienzo de la ceremonia.</li>
                    <li><i class="fa fa-bullseye"></i> La detecci�n o sospecha de abusos del sistema habilitar� a los administradores del sitio a tomar medidas para invalidar los votos correspondientes.</li>
                    <li><i class="fa fa-bullseye"></i> Este sitio no tiene fines de lucro.</li>
                    <li><i class="fa fa-bullseye"></i> Aquellas personas que participan aceptan estas bases y condiciones.</li>
                </ul>
            </div><!-- /.col-lg-9 -->
        </div><!-- /.row -->
        <div class="media-btns buttons page-scroll">
            <a href="${root}#actors">
                <div class="btn btn-default" id="play-button">Participar<i class="fa fa-play animated"></i></div>
            </a>
        </div>
    </div><!-- /.container --> 
</section>