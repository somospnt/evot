<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${not empty usuario}">
    <c:set var="idUsuario" value="${usuario.id}"/>
</c:if>
<section class="evot-seccion">
    <div class="container">
        <div class="row">
            <h1 class="section-title"><i class="fa fa-trophy" style="margin-right: 5px;"></i>Ranking
                <small>${evento.nombre}</small>
            </h1>
        </div>
        <c:if test="${(fn:length(ranking) == 0)}">
            <div class="row">
                <div class="col-md-12 advantages-item wp7 delay-05s animated fadeInDown">
                    <div class="advantages-item-icon"></div>
                    <h2 class="evot-link-ranking">Consult� tu posici�n en el ranking el Lunes 29 de Febrero a partir de las 10hs</h2>
                    <h2 class="evot-link-ranking">Aprovech� y revis� tu votaci�n</h2>
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="collapse navbar-collapse navbar-main-collapse evot-buscador-ranking">
                                <c:out value="${mensajeBuscadorVotos}"/>
                                <form class="evot-form" action="${root}/votos/usuarios" method="GET">
                                    <input type="hidden" name="eventoId" value="1"/>
                                    <div class="input-group">
                                        <input type="email" name="email" class="form-control" placeholder="Escrib� tu email para ver tu votaci�n">
                                        <div class="input-group-addon"><i class="fa fa-search"></i></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </c:if>
        <c:if test="${(fn:length(ranking) > 0)}">
            <div class="row">
                <div class="col-lg-12 wp10">
                    <div class="col-lg-12">
                        <form class="evot-input-ranking evot-form" action="${root}/eventos/${evento.id}/ranking" method="GET">
                            <div class="input-group">
                                <input type="email" name="email" class="form-control" placeholder="Buscar mi ranking por email">
                                <div class="input-group-addon"><i class="fa fa-search"></i></div>
                            </div>
                        </form></div>

                    <table class="evot-ranking">
                        <tbody>
                            <c:forEach var="lugar" items="${ranking}" varStatus="status">
                                <tr <c:if test="${idUsuario == lugar.usuario.id}">class="evot-ranking-usuario-actual"</c:if>>
                                    <td class="evot-puesto"><fmt:formatNumber pattern="00" value="${status.index + 1}"/></td>
                                    <td class="evot-nombre"><a href="${root}/eventos/${evento.id}/usuarios/${lugar.usuario.id}">${lugar.usuario.nombre}</a></td>
                                    <td class="evot-votos">${lugar.cantidadVotos} <small>ptos</small> </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </c:if>
    </div>
</section>
