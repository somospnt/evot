<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<section class="evot-seccion evot-contenedor-votos">
    <div class="container">
        <div class="row">
            <h1 class="section-title">
                <c:out value="${usuario.nombre}"/>
                <small>${evento.nombre}</small>
            </h1>
        </div>
        <div class="row actor">
            <ul class="social">
                <li><h4 class="heading">Compartir en </h4></li>
                <li><a href="#" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=www.evot.com.ar', 'sharer', 'toolbar=0, width=650, height=480');
                        return false;"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#" onclick="window.open('https://twitter.com/intent/tweet?text=Acabo de votar para los %23Oscar %23OscarEvot. Particip� vos tambi�n y gan� entradas para el cine!&url=http%3A%2F%2Fwww.evot.com.ar', 'sharer', 'toolbar=0, width=650, height=480');
                        return false;"><i class="fa fa-twitter"></i></a></li>
            </ul>
        </div>

        <div class="row">
            <c:forEach items="${votos}" var="voto">
                <div class="item">
                    <c:set var="background" value="${(empty voto.candidato.imagen) ? root.concat('/img/imagen-desconocida.png') : voto.candidato.imagen}"/>

                    <div class="col-xs-4 col-md-2 wp6 delay-05s animated fadeInUp evot-item">
                        <div class="actor evot-actor-foto" style="background-image: url('<c:out value="${background}"/>')">
                            <c:if test="${eventoTerminado}">
                                <c:if test="${voto.candidato.ganador}">
                                    <i class="fa fa-check fa-3x evot-icon-ganador-check"></i>
                                </c:if>
                                <c:if test="${!voto.candidato.ganador}">
                                    <i class="fa fa-times fa-3x evot-icon-ganador-cruz"></i>
                                </c:if>
                            </c:if>
                        </div>
                        <div class="actor-info">
                            <h4 class="heading"><c:out value="${voto.candidato.categoria.nombre}"/></h4>
                            <h5 class="sub-heading"><c:out value="${voto.candidato.nombre}"/></h5>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
        <section>
            <div class="row">
                <div class="col-md-12 advantages-item wp7 delay-05s animated fadeInDown">
                    <div class="advantages-item-icon"><i class="fa fa-trophy"></i></div>
                    <h2><a class="evot-link-ranking" href="eventos/${evento.id}/ranking">Consult� tu posici�n en el ranking el Lunes 29 de Febrero a partir de las 10hs</a></h2>
                </div>
            </div>
        </section>
    </div>

</section>