<%@page pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<html lang="es">

    <head>
        <c:set var="root" scope="request">${pageContext.request.contextPath}</c:set>
        <base href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>EVOT - Oscars 2016</title>
        <link rel="shortcut icon" href="${root}/favicon.ico" >

        <!-- Facebook og tags -->
        <meta property="fb:app_id" content="901928993172493" />
        <meta property="og:site_name" content="website" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Acabo de votar para los Oscars 2016">
        <meta property="og:url" content="http://www.evot.com.ar/">
        <meta property="og:description" content="Participá vos también y ganá entradas para el cine!">
        <meta property="og:image" content="http://evot.com.ar/img/share/facebook.jpg">

        <link href="${root}/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="${root}/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="${root}/css/style.css" rel="stylesheet">
        <link href="${root}/css/animate.css" rel="stylesheet">
        <link href="${root}/js/lib/owl-carousel/owl.carousel.css" rel="stylesheet">
        <link href="${root}/js/lib/owl-carousel/owl.theme.css" rel="stylesheet">
        <link href="${root}/js/lib/owl-carousel/owl.transitions.css" rel="stylesheet">
        <link href="${root}/js/lib/magnific-popup/magnific-popup.css" rel="stylesheet">
        <link href="${root}/js/lib/video/YTPlayer.css" rel="stylesheet">
        <link href="${root}/js/lib/flipclock/flipclock.css" rel="stylesheet">

        <link href="${root}/css/evot.css" rel="stylesheet">

    </head>

    <body id="page-top" data-spy="scroll" data-target=".navbar-custom">

        <!-- Navigation -->
        <nav class="navbar navbar-custom navbar-fixed-top wp1" role="navigation">
            <div class="container">
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                        <i class="fa fa-bars"></i>
                    </button>
                    <a class="navbar-brand" href="${root}" title="EVOT - La plataforma de votos electrónicos">
                        <img src="${root}/img/evot-logo.png" width="24" />
                        <span class="font-light">e</span>Vot
                    </a>
                </div>

                <div class="collapse navbar-collapse navbar-right navbar-main-collapse evot-navbar">
                    <ul class="nav navbar-nav">
                        <li><c:out value="${mensajeBuscadorVotos}"/></li>
                        <li>
                            <form class="evot-form" action="${root}/votos/usuarios" method="GET">
                                <input type="hidden" name="eventoId" value="1"/>
                                <div class="input-group">
                                    <input type="email" name="email" class="form-control" placeholder="Escribí tu email para ver tu votación">
                                    <div class="input-group-addon"><i class="fa fa-search"></i></div>
                                </div>
                            </form>
                        </li>
                    </ul>
                </div>


            </div>
        </nav>

        <tiles:insertAttribute name="body" />

        <!-- Contact Section -->
        <section id="contact">
            <div class="overlay"></div>
            <div class="container text-center">
                <div class="row">
                    <div class="col-lg-12 wp11">
                        <ul class="evot-social-buttons row">
                            <li class="col-xs-6 col-md-6"><a href="#"  onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=www.evot.com.ar','sharer','toolbar=0, width=650, height=480'); return false;"><i class="fa fa-facebook fa-fw"></i> <span>Facebook</span></a></li>
                            <li class="col-xs-6 col-md-6"><a href="#" onclick="window.open('https://twitter.com/intent/tweet?text=Acabo de votar para los %23Oscar %23OscarEvot. Participá vos también y ganá entradas para el cine!&url=http%3A%2F%2Fwww.evot.com.ar','sharer','toolbar=0, width=650, height=480' ); return false;"><i class="fa fa-twitter fa-fw"></i> <span>Twitter</span></a></li>
                        </ul>
                    </div><!-- /.col-lg-12 -->

                </div>
                <div class="row">

                    <div class="copyright">
                        Desarrollado e ideado por <a href="http://www.somospnt.com">PNT</a><br/>
                        El equipo de desarrollo de <a href="http://www.connectis-ict.com.ar">Connectis Argentina</a>
                    </div>
                    <div class="footer-line"></div>
                </div>
            </div>
        </section><!-- /#contact -->


        <!-- Core JavaScript Files -->
        <script src="${root}/js/jquery-1.10.2.js"></script>
        <script src="${root}/js/bootstrap.min.js"></script>
        <script src="${root}/js/jquery.easing.min.js"></script>

        <!-- JavaScript -->
        <script src="${root}/js/lib/jquery.appear.js"></script>
        <script src="${root}/js/lib/owl-carousel/owl.carousel.min.js"></script>
        <script src="${root}/js/lib/magnific-popup/jquery.magnific-popup.min.js"></script>
        <script src="${root}/js/lib/video/jquery.mb.YTPlayer.js"></script>
        <script src="${root}/js/lib/flipclock/flipclock.js"></script>
        <script src="${root}/js/lib/jquery.animateNumber.js"></script>
        <script src="${root}/js/lib/waypoints.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="${root}/js/main.js"></script>
        <script src="${root}/js/app/evot.js"></script>
        <script src="${root}/js/app/service/service.js"></script>
        <script src="${root}/js/app/service/voto/votoService.js"></script>
        <script src="${root}/js/app/ui/ui.js"></script>

        <tiles:importAttribute name="js" scope="page"/>
        <c:if test="${not empty js}">
            <script src="${root}/${js}"></script>
        </c:if>

        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-59534703-1', 'auto');
            ga('send', 'pageview');

        </script>
    </body>

</html>
