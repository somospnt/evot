evot.service.voto = (function() {

    function guardar(voto) {
        var url = evot.service.url() + "votos";
        return evot.service.post(url, voto);
    }

    return {
        guardar: guardar
    };
})();