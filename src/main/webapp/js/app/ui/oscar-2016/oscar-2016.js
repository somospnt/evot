evot.ui.oscar2016 = (function () {
    var voto = {};
    var candidatos = [];
    var idEvento = 2;
    var claseCategoriaVotada = "evot-bullet-votado";
    var owl;
    var procesandoVoto = false;
    var anchorCarousel = "#actors";

    function init() {
        initVotos();
        initCarouselCategorias();
    }

    function initVotos() {
        $("#owl-categorias").on("click", ".evot-js-categoria", function () {
            if (!procesandoVoto) {
                procesandoVoto = true;
                var $categoria = $(this),
                        idCategoria = $categoria.data("id-categoria"),
                        idCandidato = $categoria.data("id-candidato"),
                        categoriaCarousel = $("#owl-categorias").data('owlCarousel');

                $("#owl-categorias .evot-js-categoria[data-id-categoria=" + idCategoria + "]").removeClass("selected");
                $categoria.addClass("selected");
                $("#owl-categorias .owl-pagination").find(".active").addClass(claseCategoriaVotada);

                candidatos[idCategoria] = {'id': idCandidato};
                if (categoriaCarousel) {                    
                    categoriaCarousel.next(); 
                    $("html, body").animate({ scrollTop: $(anchorCarousel).offset().top }, 500);
                    if (ga) {
                        ga('send', 'event', 'oscar', 'click-voto', 'categoria [' + idCategoria + "]", idCandidato);
                    }
                }
            }
        });

        $("#enviar-votos-boton").on("click", function (event) {
            event.preventDefault();
            voto.usuario = {'nombre': $('#nombre').val(), 'email': $('#email').val()};
            voto.candidatos = [];
            candidatos.forEach(function (candidato) {
                voto.candidatos.push(candidato);
            });
            voto.idEvento = idEvento;
            console.dir(voto);
            evot.service.voto.guardar(voto)
                    .done(function (usuario) {
                        alert("Gracias por tu voto, estás participando por entradas al cine.");
                        window.location.href = evot.url() + "/votos/usuarios?eventoId=" + idEvento + "&email=" + voto.usuario.email;
                    }).fail(function (response) {
                alert(response.responseText);
                redirigirAPrimerVotoPendiente();
            });
        });
    }

    function initCarouselCategorias() {
        owl = $("#owl-categorias");
        owl.owlCarousel({
            navigation: true,
            slideSpeed: 500,
            paginationSpeed: 400,
            singleItem: true,
            mouseDrag: false,
            afterMove: function () {
                procesandoVoto = false;                                
            }
        });
        owl = owl.data('owlCarousel');
    }

    function redirigirAPrimerVotoPendiente() {
        var bullets = $("#owl-categorias .owl-pagination .owl-page");

        $.each(bullets, function (index, bullet) {
            var $bullet = $(bullet);
            if (!$bullet.hasClass(claseCategoriaVotada)) {
                owl.jumpTo(index);
                return false;
            }
        });
    }

    return {
        init: init
    };
})();

$(document).ready(function () {
    evot.ui.oscar2016.init();
});
