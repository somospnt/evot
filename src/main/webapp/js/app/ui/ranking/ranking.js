evot.ui.ranking = (function () {

    function init() {
        scrollAlUsuarioSeleccionado();
        ocultarBuscadorHeader();
    }

    function ocultarBuscadorHeader() {
        var ranking = $('.evot-puesto');
        if (ranking.length === 0) {
            $(".evot-navbar ul").hide();
        }
    }

    function scrollAlUsuarioSeleccionado() {
        var filaUsuarioActual = $(".evot-ranking-usuario-actual");
        if (filaUsuarioActual.length) {
            $('html, body').animate({
                scrollTop: $(".evot-ranking-usuario-actual").offset().top - 200
            }, 100);
        }
    }


    return {
        init: init
    };
})();

$(document).ready(function () {
    evot.ui.ranking.init();
});
