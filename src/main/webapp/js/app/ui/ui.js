evot.ui = (function() {

    var PATTERN_FECHA = "DD/MM/YYYY";
    var MAPA_DE_ACENTOS = {
        'á': 'a', 'é': 'e', 'í': 'i', 'ó': 'o', 'ú': 'u',
        'Á': 'A', 'É': 'E', 'Í': 'I', 'Ó': 'O', 'Ú': 'U'
    };

    /** Reemplaza los caracteres de acentos del parámetro por su equivalente
     * sin acento. Útil para realizar búsquedas.
     * @param {String} string el string a reemplazar los acentos.
     */
    function reemplazarAcentos(string) {
        if (!string) {
            return '';
        }
        var ret = '';
        for (var i = 0; i < string.length; i++) {
            ret += MAPA_DE_ACENTOS[string.charAt(i)] || string.charAt(i);
        }
        return ret;
    }

    /** Transforma una fecha en format String a su timestamp numerico.
     *
     * @param {String} fecha una fecha.
     * @param {String} pattern el pattern de la fecha, opcional. Default: "DD/MM/YYYY"
     * @returns {long} el timestamp de la fecha.
     */
    function stringToTimestamp(fecha, pattern) {
        var ahora = moment();
        pattern = pattern || PATTERN_FECHA;
        return moment(fecha, pattern)
                .hours(ahora.hours())
                .minutes(ahora.minutes())
                .seconds(ahora.seconds())
                .valueOf();
    }

    /** Devuelve la fecha actual en String, formateada.
     * @param {String} pattern el pattern de la fecha, opcional. Default: "DD/MM/YYYY"
     */
    function fechaActual(pattern) {
        pattern = pattern || PATTERN_FECHA;
        return moment().format(pattern);
    }

    /**
     * Denmora la ejeuccion de una funcion una cierta cantidad de tiempo (en
     * milisegundos). Si se realiza otra invocacion antes de este tiempo,
     * se cancela la primer invocación, se resetea el timer y se pone la nueva
     * invocacion en espera.
     * Por ejemplo, es útil para "ejecutar una búsqueda 2 segundos después del
     * ultimo keypress".
     * Leer mas: http://stackoverflow.com/questions/4364729/jquery-run-code-2-seconds-after-last-keypress
     *
     * @param f la funcion a invocar.
     * @param delay la demora en milisegundos. Default: 500.
     */
    function throttle(f, delay) {
        var timer = null;
        return function() {
            var context = this, args = arguments;
            clearTimeout(timer);
            timer = window.setTimeout(function() {
                f.apply(context, args);
            },
                    delay || 500);
        };
    }

    return {
        stringToTimestamp: stringToTimestamp,
        fechaActual: fechaActual,
        reemplazarAcentos: reemplazarAcentos,
        throttle: throttle
    };

})();
